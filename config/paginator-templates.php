<?php
$config = [
    'first' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'last' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="active page-item"><a class="page-link" href="#">{{text}}</a></li>',
    'nextActive' => '<li class="page-item"><a aria-label="Next" class="page-link" href="{{url}}">{{text}}</a></li>',
    'nextDisabled' => '<li class="next disabled page-item"><a class="page-link" aria-label="Next"><span aria-hidden="true">»</span></a></li>',
    'prevActive' => '<li class="page-item"><a aria-label="Previous" class="page-link" href="{{url}}">{{text}}</a></li>',
    'prevDisabled' => '<li class="prev disabled page-item"><a class="page-link" aria-label="Previous"><span aria-hidden="true">«</span></a></li>',
    'sort' => '<a href="{{url}}" class="corfe-title" style="font-size: 12px">{{text}}</a>',
    'sortAsc' => '<a href="{{url}}" class="corfe-title" style="font-size: 12px">{{text}} <i class="fa fa-arrow-up m-r-10"></i></a>',
    'sortDesc' => '<a href="{{url}}" class="corfe-title" style="font-size: 12px">{{text}} <i class="fa fa-arrow-down m-r-10"></i></a>'
];
return $config;
?>
