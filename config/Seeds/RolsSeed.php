<?php
use Migrations\AbstractSeed;

/**
 * Rols seed.
 */
class RolsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Administrador'
            ]
        ];

        $table = $this->table('rols');
        $table->insert($data)->save();
    }
}
