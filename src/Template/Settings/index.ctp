<?php
/**
 * @var \App\View\AppView                                                $this
 * @var \App\Model\Entity\Setting[]|\Cake\Collection\CollectionInterface $settings
 */
?>
<?= $this->element('index_controller', [
    'title'    => __('Settings'),
    'subtitle' => __('list_of', [__('Settings')]),
    'add_button' => true
]) ?>
<div class="row">
    <div class="col-sm-12 text-center">
        <?= $this->Flash->render('Settings') ?>
    </div>
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title"><?= __('Settings') ?></h3>
            <p class="text-muted"><?= __('list_of', [__('Settings')]) ?></p>
            <div class="table-responsive">
                <table class="table" id="dataTable">
                    <thead>
                    <tr>
                        <th><?= $this->Paginator->sort('identifier', 'Identificador') ?></th>
                        <th><?= $this->Paginator->sort('value', 'Valor') ?></th>
                        <th><?= $this->Paginator->sort('type', 'Tipo') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($settings as $setting): ?>
                        <tr>
                            <td><?= h($setting->identifier) ?></td>
                            <td><?= h($setting->value) ?></td>
                            <td><b><?= $setting->getTypes()[$setting->type] ?></b></td>
                            <td class="actions">
                                <?= $this->Html->link('<i class="fa fa-edit text-inverse m-r-10"></i>',
                                    ['action' => 'edit', $setting->id],
                                    ['escape'              => false,
                                    ]) ?>
                                <?= $this->Form->postLink('<i class="fa fa-trash text-danger"></i>',
                                    ['action' => 'delete', $setting->id],
                                    [
                                        'confirm' => __('Are you sure you want to delete # {0}?', $setting->id),
                                        'escape'  => false
                                    ]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
