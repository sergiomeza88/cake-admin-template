<?php
/**
 * @var \App\View\AppView         $this
 * @var \App\Model\Entity\Setting $setting
 */
?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Detalle de <?= __('Setting') ?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <!--<a class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></a>-->
        <?= $this->Html->link(__('Edit', [__('Setting')]),
            ['action' => 'edit', $setting->id],
            ['class' => 'btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light']) ?>
        <?= $this->Form->postLink(__('delete', [__('Setting')]),
            ['action' => 'delete', $setting->id],
            [
                'confirm' => __('Are you sure you want to delete # {0}?', $setting->id),
                'class'   => 'btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light'
            ])
        ?>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="white-box">
            <div class="">
                <h2 class="m-b-0 m-t-0">
                    <?= h($setting->id) ?>
                </h2>
                <small class="text-muted db"># <?= h($setting->id) ?></small>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table vertical-table">
                            <tr>
                                <th scope="row"><?= __('Identifier') ?></th>
                                <td><?= h($setting->identifier) ?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= __('Value') ?></th>
                                <td><?= h($setting->value) ?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= __('Type') ?></th>
                                <td><?= h($setting->type) ?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= __('Id') ?></th>
                                <td><?= $this->Number->format($setting->id) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
