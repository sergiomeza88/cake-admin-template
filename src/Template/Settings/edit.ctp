<?php
/**
 * @var \App\View\AppView         $this
 * @var \App\Model\Entity\Setting $setting
 */
?>
<?=
$this->element('index_controller', [
    'title'      => __('Settings'),
    'subtitle'   => __('add_new', [__('Setting')]),
    'add_button' => false
])
?>

<div class="row">
    <div class="panel panel-info">
        <div class="panel-heading"><?= __('edit', [__('Setting')]) ?></div>
        <div class="collapse in" aria-expanded="true">
            <div class="panel-body">
                <?= $this->Form->create($setting, ['class' => 'form-material',
                                                   "type" => "file"]) ?>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $this->Form->control('type',
                                    ['class' => 'form-control',
                                     'label' => 'Tipo de configuración',
                                     'readonly']); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $this->Form->control('identifier',
                                    ['class' => 'form-control', 'disabled',
                                     'label' => 'Identificador']); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Valor</label>
                            <div class="form-group">
                                <?php
                                switch ($setting->type){
                                    case "plain":
                                        echo $this->Form->text('value',
                                            ['class' => 'form-control', 'required',
                                             'label' => 'Valor']);
                                        break;
                                    case "text":
                                        echo $this->Form->textarea('value',
                                            ['class' => 'form-control', 'required',
                                             'label' => 'Valor', 'id' => 'editor']);
                                        break;
                                    case "number":
                                        echo $this->Form->number('value',
                                            ['class' => 'form-control', 'required',
                                             'label' => 'Valor']);
                                        break;
                                    case "file":
                                        echo $this->Form->file('value',
                                            ['class' => 'form-control', 'required',
                                             'label' => 'Valor']);
                                        break;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12 text-center center-block">
                            <?= $this->Form->button(__('Submit'),
                                ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
