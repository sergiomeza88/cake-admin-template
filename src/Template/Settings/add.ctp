<?php
/**
 * @var \App\View\AppView         $this
 * @var \App\Model\Entity\Setting $setting
 */
?>
<?=
$this->element('index_controller', [
    'title'      => __('Settings'),
    'subtitle'   => __('add_new', [__('Setting')]),
    'add_button' => false
])
?>
<div class="row">
    <div class="col-sm-12 text-center">
        <?= $this->Flash->render('setting') ?>
    </div>
</div>
<div class="row">
    <div class="panel panel-info">
        <div class="panel-heading"><?= __('add_new', [__('Setting')]) ?></div>
        <div class="collapse in" aria-expanded="true">
            <div class="panel-body">
                <?= $this->Form->create($setting, ['class' => 'form-material',
                                                   "type" => "file"]) ?>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Tipo de configuración</label>
                            <div class="form-group">
                                <?= $this->Form->select('type', $setting->getTypes(),
                                    ['class' => 'form-control', 'required' => true,
                                     'label' => 'Tipo de configuración',
                                     'id' => 'setting_type',
                                     'empty' => 'Escoge valor',
                                     'value' => $this->request->getQuery("type")]); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $this->Form->control('identifier',
                                    ['class' => 'form-control', 'required',
                                     'label' => 'Identificador']); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Valor</label>
                            <div class="form-group">
                                <?php
                                switch ($this->request->getQuery("type")){
                                    case "plain":
                                        echo $this->Form->text('value',
                                            ['class' => 'form-control', 'required',
                                             'label' => 'Valor']);
                                        break;
                                    case "text":
                                        echo $this->Form->textarea('value',
                                            ['class' => 'form-control', 'required',
                                             'label' => 'Valor', 'id' => 'editor']);
                                        break;
                                    case "number":
                                        echo $this->Form->number('value',
                                            ['class' => 'form-control', 'required',
                                             'label' => 'Valor']);
                                        break;
                                    case "file":
                                        echo $this->Form->file('value',
                                            ['class' => 'form-control', 'required',
                                             'label' => 'Valor']);
                                        break;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12 text-center center-block">
                            <?= $this->Form->button(__('Submit'),
                                ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
