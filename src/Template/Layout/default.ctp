<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Sergio Meza">
    <link rel="icon" type="image/png" sizes="16x16" href="..images/favicon.png">
    <title><?= env('APP_NAME') ?></title>

    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">
    <link rel="manifest" href="/fav/site.webmanifest">
    <link rel="mask-icon" href="/fav/safari-pinned-tab.svg" color="#31b572">
    <link rel="shortcut icon" href="/fav/favicon.ico">
    <meta name="msapplication-TileColor" content="#31b572">
    <meta name="msapplication-config" content="/fav/browserconfig.xml">
    <meta name="theme-color" content="#31b572">
    <link rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.4.2/css/all.css"
          integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns"
          crossorigin="anonymous">
    <?= $this->Html->css(
        [
            '/bower_components/bootstrap/dist/css/bootstrap.min.css',
            '/bower_components/sidebar-nav/dist/sidebar-nav.min.css',
            '/bower_components/toast-master/css/jquery.toast.css',
            '/bower_components/morrisjs/morris.css',
            '/bower_components/chartist-js/dist/chartist.min.css',
            '/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css',
            '/bower_components/dataTables/datatables.min.css',
            '/bower_components/jodit/build/jodit.min.css',
            '/bower_components/tokenize2/dist/tokenize2.min.css',
            'animate.css', 'style.css', 'colors/default.css',
            'backend.css'
        ]
    ) ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <div class="top-left-part">
                <!-- Logo -->
                <a class="logo" href="#">
                    <b>
                        <?= $this->Html->image('/fav/favicon-32x32.png', ['class' => 'dark-logo']) ?>
                        <?= $this->Html->image('/fav/favicon-32x32.png', ['class' => 'light-logo']) ?>
                    </b>
                </a>
            </div>
            <ul class="nav navbar-top-links navbar-left">
                <li>
                    <a href="javascript:void(0)"
                       class="open-close waves-effect waves-light">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
            <!-- /Logo -->
            <ul class="nav navbar-top-links navbar-right pull-right">
                <li>
                    <?= $this->Html->link("Ver sitio", "/", [
                        "target" => "_blank"
                    ]) ?>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                        <b class="hidden-xs"><?= $user['name']; ?></b>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-user animated flipInY">
                        <li>
                            <div class="dw-user-box">
                                <div class="u-text">
                                    <h4><?= $user['name']; ?></h4>
                                    <p class="text-muted">
                                        <?= $user['email'] ?>
                                    </p>
                                    <a href="/perfil" class="btn btn-rounded btn-danger btn-sm">
                                        <?= __('view_profile') ?>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <?= $this->Html->link('<i class="fa fa-power-off fa-fw" aria-hidden="true"></i> '
                                . __('logout'),
                                ['action' => 'logout', 'controller' => 'Users'],
                                ['class' => '', 'escape' => false]) ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav slimscrollsidebar">
            <div class="sidebar-head">
                <h3>
                    <span class="fa-fw open-close">
                        <i class="fa fa-times ti-menu"></i>
                    </span>
                    <span class="hide-menu">Navegación</span>
                </h3>
            </div>
            <ul class="nav" id="side-menu">
                <li style="padding: 70px 0 0;">
                    <a href="<?= env('ROOT') ?>/admin" class="waves-effect">
                        <i class="fa fa-home" aria-hidden="true"></i>Home
                    </a>
                </li>
                <?php foreach ($menu as $it): ?>
                    <li class="menu-item">
                        <a href="javascript:void(0);" class="waves-effect">
                            <i class="fa <?= $it['icon'] ?>" aria-hidden="true">
                                <?= $it['icon'] ? '' : substr($it['name'], 0, 2) ?>
                            </i>
                            <span class="hide-menu">
                            <?= $it['name'] ?><span class="fa arrow"></span>
                        </span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <?= $this->Html->link(
                                    '<i class="fa fa-list" aria-hidden="true"></i>
                                        <span class="hide-menu">
                                            ' . __("list_of", [$it['name']]) . '
                                        </span>',
                                    ['controller' => $it['controller'], 'action' => 'index'],
                                    ['escape' => false]
                                ); ?>
                            </li>
                            <?php if($user['rol_id'] != 3): ?>
                                <li>
                                    <?= $this->Html->link(
                                        '<i class="fa fa-plus-circle"></i>
                                        <span class="hide-menu">
                                            ' . $it["new"] . '
                                        </span>',
                                        ['controller' => $it['controller'], 'action' => 'add'],
                                        ['escape' => false]
                                    ); ?>
                                </li>
                            <?php
                            endif;
                            if ($it['controller'] == 'Settings'): ?>

                            <?php
                            endif;
                            ?>
                        </ul>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <?= $this->Flash->render('auth') ?>
            <?= $this->fetch('content') ?>
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"><?= date('Y') . ' &copy; Desarrollado por ' . env('AUTHOR') ?></footer>
    </div>
</div>
<?= $this->Html->script(
    [
        '/bower_components/jquery/dist/jquery.min.js',
        '/bower_components/bootstrap/dist/js/bootstrap.min.js',
        '/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
        'jquery.slimscroll.js', 'waves.js',
        '/bower_components/waypoints/lib/jquery.waypoints.js',
        '/bower_components/counterup/jquery.counterup.min.js',
        '/bower_components/chartist-js/dist/chartist.min.js',
        '/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js',
        '/bower_components/jquery-sparkline/jquery.sparkline.min.js',
        '/bower_components/toast-master/js/jquery.toast.js',
        '/bower_components/dataTables/datatables.min.js',
        '/bower_components/tokenize2/dist/tokenize2.min.js',
        '/bower_components/jodit/build/jodit.min.js', 'custom.js',
        'dashboard1.js'
    ]
) ?>
</body>

</html>
