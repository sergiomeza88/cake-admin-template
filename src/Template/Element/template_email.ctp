<?php
/**
 * @var \App\Model\Entity\Provider $provider
 * @var \App\Model\Entity\Contract $contract
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width">
<style type="text/css">
    /*-------------------- client-specific styles & reset --------------------*/

    #outlook a {
        padding: 0;
    }

    body, table.body, h1, h2, h3, h4, h5, h6, table, tr, td, div, hr {
        padding: 0;
        margin: 0;
    }

    body,
    table.body {
        width: 100% !important;
        height: 100% !important;
        min-width: 100%;
        line-height: 100% !important;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
    }

    .ExternalClass {
        width: 100%;
    }

    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
        line-height: 100%;
    }

    table,
    td {
        border-collapse: collapse !important;
    }

    table {
        max-width: 100%;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        border-spacing: 0;
    }

    table, tr, td {
        vertical-align: top;
        text-align: left;
    }

    center {
        width: 100%;
        min-width: 0 !important;
    }

    img {
        max-width: 100%;
        height: auto;
        border: 0 none;
        line-height: 100%;
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
    }

    a img {
        border: 0 none;
    }

    hr {
        border: 0 none;
    }


    /*-------------------- elements --------------------*/

    body,
    table.body {
        background: #f7f7f7;
    }

    body, table.body, h1, h2, h3, h4, h5, h6, p, td, div, br {
        font-family: "Helvetica", "Arial", sans-serif;
        font-weight: normal;
        font-size: 15px;
        line-height: 18px;
        color: #333;
    }

    h1, h2, h3, h4, h5, h6 {
        color: #333 !important;
    }

    h1 {
        font-size: 18px;
        line-height: 22px;
    }

    h2 {
        font-size: 15px;
        font-weight: bold;
    }

    a:link,
    a:visited,
    a:hover,
    a:active {
        color: #03a9f4 !important;
        text-decoration: none;
    }

    h1 a:link,
    h2 a:link,
    h3 a:link,
    h4 a:link,
    h5 a:link,
    h6 a:link,
    a.black:link,

    h1 a:visited,
    h2 a:visited,
    h3 a:visited,
    h4 a:visited,
    h5 a:visited,
    h6 a:visited,
    a.black:visited,

    h1 a:hover,
    h2 a:hover,
    h3 a:hover,
    h4 a:hover,
    h5 a:hover,
    h6 a:hover,
    a.black:hover,

    h1 a:active,
    h2 a:active,
    h3 a:active,
    h4 a:active,
    h5 a:active,
    h6 a:active,
    a.black:active {
        color: #333 !important;
    }

    hr {
        height: 1px;
        background: #bbb;
        border: 0;
    }


    /*-------------------- buttons --------------------*/

    .button {
        width: 100%;
        min-width: 100%;
        overflow: hidden;
        border-collapse: separate !important;
    }
    table.button {
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        -ms-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        -ms-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
    }

    .button td {
        display: block;
        width: auto !important;
        overflow: hidden;
        padding: 0 10px;
        background: #03a9f4;
        color: #fff;
        text-align: center;
    }
    .button.button-green td {
        background: #00b776;
    }

    .button td a.button-link:link,
    .button td a.button-link:hover,
    .button td a.button-link:visited,
    .button td a.button-link:active {
        display: inline-block;
        width: 100%;
        border-width: 12px 0;
        border-style: solid;
        border-color: #03a9f4;
        color: #fff !important;
        text-decoration: none;
    }
    .button.button-green td a.button-link:link,
    .button.button-green td a.button-link:hover,
    .button.button-green td a.button-link:visited,
    .button.button-green td a.button-link:active {
        border-color: #00b776;
    }


    /*-------------------- common --------------------*/

    table.table td {
        padding-top: 10px;
    }

    .center {
        text-align: center;
    }

    img.center {
        margin: 0 auto;
    }

    img.round {
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
    }

    .preview-text {
        font-size: 1px;
        line-height: 1px;
        color: #f7f7f7;
    }

    .contains-long-links {
        word-break: break-all;
    }

    .p {
        margin-top: 13px;
        font-size: 14px;
        line-height: 21px;
        color: #777;
    }

    .p.note {
        font-size: 12px;
        line-height: 18px;
    }

    .checkmark {
        font-size: 24px;
        color: #00b776;
    }

    .outer {
        width: 448px;
        margin: 0 auto;
    }

    @media only screen and (max-width: 448px) {
        table[class="body"] .outer {
            width: 100% !important;
        }
    }

    .inner {
        width: 306px;
        margin: 0 auto;
        position: relative;
    }

    @media only screen and (max-width: 320px) {
        table[class="body"] .inner {
            width: 95% !important;
        }
    }


    /*-------------------- lists --------------------*/

    .list {
        width: 100%;
        min-width: 100%;
    }

    .list td {
        padding-top: 17px;
        padding-bottom: 2px;
        vertical-align: top;
    }

    .list td.bullet {
        width: 30px;
        text-align: center;
    }


    /*-------------------- header --------------------*/

    .header {
        padding-top: 13px;
        padding-bottom: 7px;
    }


    /*-------------------- content --------------------*/

    .content-wrapper {
        background: #fff;
    }

    .content {
        padding-top: 20px;
        padding-bottom: 28px;
        position: relative;
    }

    .problem-text {
        margin-top: 18px;
    }

    .problem-text .p {
        margin-top: 10px;
        font-size: 18px;
        line-height: 22px;
        color: #333;
    }


    /*-------------------- choices --------------------*/

    .choices td {
        vertical-align: middle;
        padding-top: 9px;
        padding-bottom: 5px;
    }

    .choices td.radiobutton {
        width: 28px;
        padding-left: 12px;
        padding-right: 12px;
    }

    .choices td a:link,
    .choices td a:visited,
    .choices td a:hover,
    .choices td a:active {
        color: #333 !important;
    }

    .follower td {
        vertical-align: middle;
    }

    .follower td.avatar {
        width: 42px;
        padding-left: 4px;
        padding-right: 18px;
    }
    @media only screen and (max-width: 240px) {
        table[class="body"] .follower-stats {
            width: 100% !important;
            min-width: 0 !important;
        }
    }

    .follower-stats td {
        font-size: 13px;
        color: #777;
    }

    .follower-stats td.following,
    .follower-stats td.followers {
        width: 48%;
    }

    .follower-stats td.following {
        text-align: left;
    }

    .follower-stats td.followers {
        text-align: right;
    }

    .follower-stats td.spacer {
        width: 4%;
    }

    .follower-stats td strong {
        color: #333;
    }

    .form-buttons td {
        font-size: 14px;
    }

    .form-buttons td.view-solutions,
    .form-buttons td.submit {
        width: 48%;
        padding-top: 14px;
    }

    .form-buttons td.spacer {
        width: 4%;
    }

    .answer .button {
        border: 1px solid #777;
    }

    .answer .button,
    .answer .button td {
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }

    .answer .button td {
        background: #f7f7f7;
        color: #777;
        text-align: left;
    }

    .answer .button td a.button-link:link,
    .answer .button td a.button-link:hover,
    .answer .button td a.button-link:visited,
    .answer .button td a.button-link:active {
        border-width: 11px 0;
        border-color: #f7f7f7;
        color: #777 !important;
    }

    .view-solutions .button {
        border: 1px solid #333;
    }

    .view-solutions .button td {
        background: #fff;
        color: #333;
    }

    .view-solutions .button td a.button-link:link,
    .view-solutions .button td a.button-link:hover,
    .view-solutions .button td a.button-link:visited,
    .view-solutions .button td a.button-link:active {
        border-width: 11px 0;
        border-color: #fff;
        color: #333 !important;
    }

    .submit .button td {
        border: 1px solid #03a9f4;
    }

    .submit .button td a.button-link:link,
    .submit .button td a.button-link:hover,
    .submit .button td a.button-link:visited,
    .submit .button td a.button-link:active {
        border-width: 11px 0;
    }

    /*-------------------- mobile app badges --------------------*/

    .mobile-app-badges {
        width: 100%;
        min-width: 100%;
    }

    .mobile-app-badges td {
        padding-top: 28px;
        line-height: 10px;
    }

    /*-------------------- footer --------------------*/

    .text-footer {
        color: #333;
    }

    .footer {
        padding-top: 14px;
        padding-bottom: 14px;
        font-size: 12px;
        line-height: 14px;
        color: #bbb;
        text-align: center;
    }

    .footer a:link,
    .footer a:visited,
    .footer a:hover,
    .footer a:active,
    a.footer-link:link,
    a.footer-link:visited,
    a.footer-link:hover,
    a.footer-link:active {
        color: #bbb !important;
    }

    .footer a,
    a.footer-link {
        text-decoration: underline;
    }
    .footer a:hover,
    a.footer-link:hover {
        text-decoration: none;
    }

    .potw-difficulty-table td {
        width: 33.33%;
    }
</style>
<style>
    .pd {  padding: 5px;  }
    .success {  background: #82ad2b;  }
    .error {  background: rgba(250, 0, 0, 0.6);  }
    .problem-content hr { display: none; }
</style>
<table class="body" style="width: 100% !important; height: 100% !important; min-width: 100%; line-height: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; border-collapse: collapse !important; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; vertical-align: top; text-align: left; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 15px; color: #333; background: #f7f7f7; margin: 0; padding: 0;"
       bgcolor="#f7f7f7">
    <tr style="vertical-align: top; text-align: left; margin: 0; padding: 0;" align="left">
        <td class="center" align="center" valign="top" style="border-collapse: collapse !important; vertical-align: top; text-align: center; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 15px; line-height: 18px; color: #333; margin: 0; padding: 0;">
            <center style="width: 100%; min-width: 0 !important;">
                <center style="width: 100%; min-width: 0 !important;">
                    <div class="preview-text center" style="font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal;
                    font-size: 1px; line-height: 1px; color: #f7f7f7; text-align: center; margin: 0; padding: 0;"
                         align="center"><?= $description ?></div>
                </center>
                <table class="outer" style="border-collapse: collapse !important; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; vertical-align: top; text-align: left; width: 448px; margin: 0 auto; padding: 0;">
                    <tr style="vertical-align: top; text-align: left; margin: 0; padding: 0;" align="left">
                        <td class="header center" align="center" style="border-collapse: collapse !important; vertical-align: top; text-align: center; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 15px; line-height: 18px; color: #333; margin: 0; padding: 13px 0 7px;"
                            valign="top">
                            <center style="width: 100%; min-width: 0 !important;">
                                <a href="http://corfecali.com.co/" style="color: #03a9f4 !important; text-decoration: none;">
                                    <img class="center" src="https://i1.wp.com/corfecali.com.co/core/wp-content/uploads/2017/07/corfecali-logo.png?"
                                         width="240" border="0" style="max-width: 100%; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; text-align: center; margin: 0 auto; border: 0 none;">
                                </a>
                                <p style="font-size: 20px; font-weight: bold; line-height: 1.2em; margin-bottom: 14px; font-family: 'Helvetica', 'Arial', sans-serif; color: #333;">
                                    <span>
                                        <?= $head ?>
                                    </span>
                                </p>
                            </center>
                        </td>
                    </tr>
                </table>
                <table class="outer" style="border-collapse: collapse !important; max-width: 100%; mso-table-lspace: 0pt;
                mso-table-rspace: 0pt; border-spacing: 0; vertical-align: top; text-align: left; width: 550px; margin: 0 auto; padding: 0;">
                    <tr style="vertical-align: top; text-align: left; margin: 0; padding: 0;" align="left">
                        <td class="content-wrapper center problem-content" align="center" style="border-collapse: collapse !important; vertical-align: top; text-align: center; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 15px; line-height: 18px; color: #333; background: #fff; margin: 0; padding: 0;"
                            bgcolor="#fff" valign="top">
                            <center style="width: 100%; min-width: 0 !important;">
                                <table class="inner" style="border-collapse: collapse !important; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; vertical-align: top; text-align: left; width: 450px; position: relative; margin: 0 auto; padding: 0;">
                                    <tr style="vertical-align: top; text-align: left; margin: 0; padding: 0;" align="left">
                                        <td class="content" style="border-collapse: collapse !important; vertical-align: top; text-align: left; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 15px; line-height: 18px; color: #333; position: relative; margin: 0; padding: 20px 0 28px;"
                                            align="left" valign="top">
                                            <br style="font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 15px; line-height: 18px; color: #333;">
                                            <div class="problem-text" style="font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 15px; line-height: 18px; color: #333; margin: 18px 0 0; padding: 0;">
                                                <div class="p" style="font-family: 'Helvetica', 'Arial', sans-serif;
                                                    font-weight: normal; font-size: 17px; line-height: 22px; color: #5d5d5d;
                                                    margin: 10px 0 0; padding: 0; font-weight: 100">
                                                    <p style="text-align: justify;">
                                                        <?= $content ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </td>
                    </tr>
                </table>
                <?php
                $social = array(
                    array(
                        "icon" => "https://cdn1.iconfinder.com/data/icons/iconza-circle-social/64/697057-facebook-64.png",
                        "url" => "https://www.facebook.com/Corfecali/"
                    ),
                    array(
                        "icon" => "https://cdn1.iconfinder.com/data/icons/iconza-circle-social/64/697029-twitter-128.png",
                        "url" => "https://twitter.com/corfecali/"
                    )
                );
                ?>
                <table class="outer" style="border-collapse: collapse !important; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; vertical-align: top; text-align: left; width: 550px; margin: 0 auto; padding: 0;">
                    <tr style="vertical-align: top; text-align: left; margin: 0; padding: 0;" align="left">
                        <td class="content-wrapper center" align="center" style="border-top-width: 1px; border-top-color: #F0F0F0; border-top-style: solid; border-collapse: collapse !important; vertical-align: top; text-align: center; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 15px; line-height: 18px; color: #333; background: #fff; margin: 0; padding: 0;"
                            bgcolor="#fff" valign="top">
                            <center style="width: 100%; min-width: 0 !important;">
                                <table class="inner" style="border-collapse: collapse !important; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; vertical-align: top; text-align: left; width: 306px; position: relative; margin: 0 auto; padding: 0;">
                                    <tr style="vertical-align: top; text-align: left; margin: 0; padding: 0;" align="left">
                                        <td class="content" style="border-collapse: collapse !important; vertical-align: top; text-align: left; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 15px; line-height: 18px; color: #333; position: relative; margin: 0; padding: 0 0 28px;"
                                            align="left" valign="top">
                                            <table class="mobile-app-badges" style="border-collapse: collapse !important; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; vertical-align: top; text-align: left; width: 100%; min-width: 100%; margin: 0; padding: 0;">
                                                <tr style="vertical-align: top; text-align: left; margin: 0; padding: 0;" align="left">
                                                    <?php foreach ($social as $soc): ?>
                                                        <td style="border-collapse: collapse !important; vertical-align: top; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 15px; line-height: 10px; color: #333; width: 33%; margin: 0; padding: 28px 0 0; text-align: center;" valign="top">
                                                            <a href="<?= $soc['url']?>" style="color: #03a9f4 !important; text-decoration: none;">
                                                                <img src="<?= $soc['icon'] ?>" width="64"
                                                                     height="64" border="0" alt="Brilliant"
                                                                     style="max-width: 100%; height: auto;
                                                                 line-height: 100%; outline: none; text-decoration: none;
                                                                 -ms-interpolation-mode: bicubic; border: 0 none;">
                                                            </a>
                                                        </td>
                                                    <?php endforeach; ?>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <!--// inner -->
                            </center>
                        </td>
                    </tr>
                </table>
                <!--// content -->
                <table class="outer" style="border-collapse: collapse !important; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; vertical-align: top; text-align: left; width: 448px; margin: 0 auto; padding: 0;">
                    <tr style="vertical-align: top; text-align: left; margin: 0; padding: 0;" align="left">
                        <td class="center" align="center" style="border-collapse: collapse !important; vertical-align: top; text-align: center; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 15px; line-height: 18px; color: #333; margin: 0; padding: 0;"
                            valign="top">
                            <center style="width: 100%; min-width: 0 !important;">
                                <table class="inner" style="border-collapse: collapse !important; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; vertical-align: top; text-align: left; width: 306px; position: relative; margin: 0 auto; padding: 0;">
                                    <tr style="vertical-align: top; text-align: left; margin: 0; padding: 0;" align="left">
                                        <td class="footer center" align="center" style="border-collapse: collapse !important; vertical-align: top; text-align: center; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; font-size: 12px; line-height: 14px; color: #bbb; margin: 0; padding: 14px 0;"
                                            valign="top">
                                            <center style="width: 100%; min-width: 0 !important;">
                                                Mensaje enviado automaticamente
                                            </center>
                                        </td>
                                    </tr>
                                </table>
                                <!--// inner -->
                            </center>
                        </td>
                    </tr>
                </table>
                <!--// footer -->
            </center>
        </td>
    </tr>
</table>
<!--// body -->
</body>
</html>

