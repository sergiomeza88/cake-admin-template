<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?= $title?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <?= isset($add_button) ? $add_button ? $this->Html->link(__('add_new', [$title]),
            ['action' => 'add'],
            ['class' => 'btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light']) : '' : '' ?>
        <?= isset($edit_button) ? $edit_button ? $this->Html->link(__('edit', [$title]),
            ['action' => 'edit', $id],
            ['class' => 'btn btn-info pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light']) : '' : ''?>
        <?= isset($delete_button) ? $delete_button ? $this->Form->postLink(__('delete', [$title]),
            ['action' => 'delete', $id],
            ['confirm' => __('Are you sure you want to delete # {0}?', $id),
             'class' => 'btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light']) : '' : ''?>
        <ol class="breadcrumb">
            <li><a href="#"><?= $title ?></a></li>
            <li class="active"><?= $subtitle ?></li>
        </ol>
    </div>
</div>
