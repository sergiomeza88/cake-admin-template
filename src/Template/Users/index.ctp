<?php
/**
 * @var \App\View\AppView                                             $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<?= $this->element('index_controller', [
    'title'    => __('Users'),
    'subtitle' => __('list_of', [__('Users')]),
    'add_button' => true
]) ?>
<div class="row">
    <div class="col-sm-12 text-center">
        <?= $this->Flash->render('users') ?>
    </div>
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title"><?= __('Users') ?></h3>
            <p class="text-muted"><?= __('list_of', [__('Users')]) ?></p>
            <div class="table-responsive">
                <table class="table" id="dataTable">
                    <thead>
                    <tr>
                        <th><?= $this->Paginator->sort('id') ?></th>
                        <th><?= $this->Paginator->sort('name') ?></th>
                        <th><?= $this->Paginator->sort('email') ?></th>
                        <th><?= $this->Paginator->sort('rol_id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $user): ?>
                        <tr>
                            <td><?= $this->Number->format($user->id) ?></td>
                            <td><?= h($user->name) ?></td>
                            <td><?= h($user->email) ?></td>
                            <td><?= $user->has('rol') ? $this->Html->link('<span class="label label-info">'. $user->rol->name .'</span>',
                                            ['controller' => 'Rols', 'action' => 'view', $user->rol->id],
                                            ['escape' => false]): '' ?></td>
                            <td class="actions">
                                <?= $this->Html->link('<i class="fa fa-eye text-inverse m-r-10"></i>',
                                    ['action' => 'view', $user->id],
                                    ['escape' => false]) ?>
                                <?= $this->Html->link('<i class="fa fa-pencil text-inverse m-r-10"></i>',
                                    ['action' => 'edit', $user->id],
                                    ['escape'              => false,
                                     'data-toggle'         => 'tooltip',
                                     'data-original-title' => __('edit', [''])
                                    ]) ?>
                                <?= $this->Form->postLink('<i class="fa fa-trash text-danger"></i>',
                                    ['action' => 'delete', $user->id],
                                    [
                                        'confirm' => __('Are you sure you want to delete # {0}?', $user->id),
                                        'escape'  => false
                                    ]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
