<?php
/**
 * @var \App\View\AppView $this
 */
?>
<html lang="es">
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="..images/favicon.png">
    <title><?= env('APP_NAME') ?></title>

    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">
    <link rel="manifest" href="/fav/site.webmanifest">
    <link rel="mask-icon" href="/fav/safari-pinned-tab.svg" color="#31b572">
    <link rel="shortcut icon" href="/fav/favicon.ico">
    <meta name="msapplication-TileColor" content="#31b572">
    <meta name="msapplication-config" content="/fav/browserconfig.xml">
    <meta name="theme-color" content="#31b572">

    <?= $this->Html->css(
        [
            '/bower_components/bootstrap/dist/css/bootstrap.min.css',
            '/bower_components/sidebar-nav/dist/sidebar-nav.min.css',
            '/bower_components/toast-master/css/jquery.toast.css',
            '/bower_components/morrisjs/morris.css',
            '/bower_components/chartist-js/dist/chartist.min.css',
            '/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css',
            'animate.css', 'style.css', 'colors/default.css'
        ]
    )?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- Preloader -->
    <div class="preloader" style="display: none;">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
        </svg>
    </div>
    <section id="wrapper" class="new-login-register">
        <div class="lg-info-panel">
            <div class="inner-panel">
                <div class="lg-content">
                    <h2><?= env('APP_NAME') ?></h2>
                </div>
            </div>
        </div>
        <div class="new-login-box">
            <div class="white-box">
                <h3 class="box-title m-b-0"><?= env('APP_NAME') ?></h3>
                <?= $this->Form->create(null, [
                    'class' => 'form-horizontal new-lg-form form-material',
                    'id' => 'loginform']) ?>
                    <div class="form-group  m-t-20">
                        <div class="col-xs-12">
                            <?= $this->Form->control('email',
                                ['class' => 'form-control', 'type' => 'email',
                                 'required']) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <?= $this->Form->control('password',
                                ['class' => 'form-control', 'type' => 'password',
                                 'required']) ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <a href="javascript:void(0)" id="to-recover"
                               class="text-dark pull-right">
                                <i class="fa fa-lock m-r-5"></i> <?= __('forgot_pass_title')?>
                            </a>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <?= $this->Form->button(__('login'),[
                                'class' => 'btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light'
                            ]) ?>
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </section>
    <?= $this->Html->script(
        [
            '/bower_components/jquery/dist/jquery.min.js',
            '/bower_components/bootstrap/dist/js/bootstrap.min.js',
            '/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
            'jquery.slimscroll.js',
            'waves.js',
            '/bower_components/waypoints/lib/jquery.waypoints.js',
            '/bower_components/counterup/jquery.counterup.min.js',
            '/bower_components/chartist-js/dist/chartist.min.js',
            '/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js',
            '/bower_components/jquery-sparkline/jquery.sparkline.min.js',
            'custom.min.js',
            'dashboard1.js',
            '/bower_components/toast-master/js/jquery.toast.js'
        ]
    )?>
</body>
</html>
