<?php
/**
 * @var \App\View\AppView      $this
 * @var \App\Model\Entity\User $user
 */
?>
<?=
$this->element('index_controller', [
    'title'      => __('Users'),
    'subtitle'   => __('add_new', [__('User')]),
    'add_button' => false
])
?>
<div class="row">
    <div class="col-sm-12 text-center">
        <?= $this->Flash->render('User') ?>
    </div>
</div>
<div class="row">
    <div class="panel panel-info">
        <div class="panel-heading"><?= __('add_new', [__('User')]) ?></div>
        <div class="collapse in" aria-expanded="true">
            <div class="panel-body">
                <?= $this->Form->create($user, ['class' => 'form-material']) ?>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $this->Form->control('name',
                                    ['class' => 'form-control', 'required']); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $this->Form->control('email',
                                    ['class' => 'form-control', 'required']); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $this->Form->control('password',
                                    ['class' => 'form-control', 'required']); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= $this->Form->control('rol_id',
                                    [
                                        'options' => $rols,
                                        'id' => 'sel-rol',
                                        'class'   => 'form-control',
                                        'required',
                                        'value' => $this->request->getQuery('rol_id')
                                    ]); ?>
                            </div>
                        </div>
                    </div>
                    <!-- Rol Estudiante -->
                    <?php if ($this->request->getQuery("rol_id") == 2): ?>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= $this->Form->control('user_meta.phone',
                                        ['class' => 'form-control']); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= $this->Form->control('user_meta.nationality',
                                        ['class' => 'form-control']); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label><?= __('age') ?></label>
                                <div class="form-group">
                                    <?= $this->Form->number('user_meta.age',
                                        ['class' => 'form-control']); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12 text-center center-block">
                            <?= $this->Form->button(__('Submit'),
                                ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
