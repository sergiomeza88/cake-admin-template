<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Detalle de <?= __('User')?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <!--<a class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></a>-->
        <?= $this->Html->link(__('Edit', [__('User')]),
            ['action' => 'edit', $user->id],
            ['class' => 'btn btn-primary pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light']) ?>
        <?= $this->Form->postLink(__('delete', [__('User')]),
            ['action' => 'delete', $user->id],
            ['confirm' => __('Eliminar usuario # {0}?', $user->id),
             'class' => 'btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light'])
        ?>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="white-box">
            <div class="">
                <h2 class="m-b-0 m-t-0">
                    <?= h($user->name) ?>
                </h2>
                <small class="text-muted db"># <?= h($user->name) ?></small>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table vertical-table">
                            <tr>
                                <th scope="row"><?= __('Id') ?></th>
                                <td><?= $this->Number->format($user->id) ?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= __('Name') ?></th>
                                <td><?= h($user->name) ?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= __('Email') ?></th>
                                <td><?= h($user->email) ?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= __('Rol') ?></th>
                                <td><?= $user->has('rol') ? $this->Html->link($user->rol->name, ['controller' => 'Rols', 'action' => 'view', $user->rol->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= __('Created At') ?></th>
                                <td><?= h($user->created_at) ?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= __('Updated At') ?></th>
                                <td><?= h($user->updated_at) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <hr>
                <?php if ($user->user_meta): ?>
                <h2 class="m-b-0 m-t-0">
                    Datos Extra
                </h2>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table vertical-table">
                            <tr>
                                <th scope="row"><?= __('Phone') ?></th>
                                <td><?= $user->user_meta[0]->phone ?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= __('Nationality') ?></th>
                                <td><?= $user->user_meta[0]->nationality ?></td>
                            </tr>
                            <tr>
                                <th scope="row"><?= __('Age') ?></th>
                                <td><?= $user->user_meta[0]->age ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php endif; ?>
                <?php if ($user->user_location): ?>
                <h2 class="m-b-0 m-t-0">
                    Datos de localización
                </h2>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table vertical-table">
                            <thead>
                            <tr>
                                <th>Latitud</th>
                                <th>Longitud</th>
                                <th>Creado</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($user->user_location as $loc): ?>
                            <tr>
                                <td><?= $loc->latitude ?></td>
                                <td><?= $loc->longitude ?></td>
                                <td><?= $loc->created_at->nice() ?></td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
