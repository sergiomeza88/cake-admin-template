<?php
namespace App\Model\Behavior;

use App\Controller\AppController;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use Cake\Utility\Text;

/**
 * DeleteImage Behavior
 */
class DeleteImageBehavior extends Behavior
{
    protected $_defaultConfig = [
        'field' => 'imagen',
    ];

    //Cuando se edita, si hay imagen se borra la original (anterior)
    public function afterSaveCommit(Event $event, Entity $entity,
                                    ArrayObject $options){
        if (!$entity->isNew()){
            $field = $this->getConfig('field');
            $newValue = $entity->get($field);
            $value = $entity->getOriginal($field);
            if ($newValue != $value){
                AppController::deleteFile($value);
            }
        }
    }

    //Cuando se elimina
    public function afterDelete(Event $event, EntityInterface $entity,
                                ArrayObject $options){
        $field = $this->getConfig('field');
        $value = $entity->get($field);
        AppController::deleteFile($value);
    }
}
