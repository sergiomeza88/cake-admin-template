<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Setting Entity
 *
 * @property int $id
 * @property string $identifier
 * @property string $value
 * @property string $type
 */
class Setting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'identifier' => true,
        'value' => true,
        'type' => true
    ];

    public function getTypes(){
        return [
            "plain" => "Texto plano",
            "text" => "Texto",
            "number" => "Número",
            "file" => "Archivo"
        ];
    }
}
