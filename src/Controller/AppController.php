<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect'  => [
                'controller' => 'Users',
                'action'     => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action'     => 'login'
            ],
            'authError'      => 'No tienes permiso para realizar esta acción',
            'authenticate'   => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'authorize' => 'Controller',
        ]);

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        $this->loadComponent('Security');
        $this->loadComponent('Csrf');
        $this->Auth->setConfig('flash', [
            'key' => 'auth',
            'element' => 'error'
        ]);
        if ($this->Auth->user()) {
            if ($this->Auth->user('rol_id') == 1) {
                $menu = [
                    [
                        'icon'       => 'fa-users',
                        'name'       => __('Users'),
                        'new' => 'Nuevo ' . __('User'),
                        'controller' => 'Users'
                    ],
                    [
                        'icon'       => 'fa-cog',
                        'name'       => __('Settings'),
                        'new' => 'Nuevo ' . __('Setting'),
                        'controller' => 'Settings'
                    ],
                ];
                $this->set(compact('menu'));
            }
            $this->set('user', $this->Auth->user());
        }
    }

    /**
     * @param $data
     * JSON DATA FOR REST API
     */
    public Static function encode($data)
    {
        echo json_encode($data, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_QUOT);
    }

    /**
     * @param      $titulo
     * @param      $template
     * @param      $vars
     * @param      $to
     * @param null $attach
     * @param null $from
     *
     * @internal param $em
     */
    public static function sendEmail($titulo, $template,
                                     $vars, $to, $attach = null,
                                     $from = null)
    {
        $email = new Email();
        $email->setFrom($from != null ? $from : env('MAIL_USER'))
              ->addTo($to)
              ->addAttachments($attach)
              ->setSubject($titulo)
              ->setTemplate($template)
              ->setViewVars($vars)
              ->setEmailFormat('html')
              ->send();
    }

    public function isAuthorized($user)
    {
        if ($user['type'] == 'admin') {
            switch ($this->Auth->user('rol_id')) {
                case 2: //Editor
                    if ($this->request->getParam('action') === 'delete') {
                        return false;
                    }
                    break;
                case 3: //Consultor
                    if ($this->request->getParam('action') === 'add' ||
                        $this->request->getParam('action') === 'edit' ||
                        $this->request->getParam('action') === 'delete') {
                        return false;
                    }
                    break;
            }

            return true;
        } else {
            //Autorizaciones para el proveedor
            //if ($this->request->getParam('action') === 'logout' ||
            //    $this->request->getParam('action') === 'deleteProviderEmail' ||
            //    $this->request->getParam('action') ===  'deleteProviderPhone' ||
            //    $this->request->getParam('action') ===  'updateProvider' ||
            //    $this->request->getParam('action') ===  'uploadContractCoti' ||
            //    $this->request->getParam('action') ===  'deleteProviderTipoServicio'){
            //    return true;
            //}
            return false;
        }
    }
}
