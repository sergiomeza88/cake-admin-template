<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;

/**
 * Settings Controller
 *
 * @property \App\Model\Table\SettingsTable $Settings
 *
 * @method \App\Model\Entity\Setting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SettingsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $settings = $this->Settings->find("all");
        $this->set(compact('settings'));
    }

    /**
     * View method
     *
     * @param string|null $id Setting id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $setting = $this->Settings->get($id, [
            'contain' => []
        ]);

        $this->set('setting', $setting);
    }

    /**
    * Add method
    *
    * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
    */
    public function add()
    {
        $setting = $this->Settings->newEntity();
        if ($this->request->is('post')) {
            $setting = $this->Settings->patchEntity($setting, $this->request->getData());
            if ($setting->type == "file"){
                $file = $this->request->getData("value");
                if ($file) {
                    $ext          = pathinfo($file['name'], PATHINFO_EXTENSION);
                    $file['name'] = Text::uuid() . '.' . $ext;
                    $uri          = 'files/settings/';
                    if (AppController::uploadFile($file, $uri)) {
                        $setting->value = $uri . $file['name'];
                    }
                } else {
                    $this->Flash->error('Sin archivo adjuntado',
                        ["key" => "setting"]);
                }
            }
            if ($this->Settings->save($setting)) {
                $this->Flash->success(__('The setting has been saved.'), [
                    'key' => 'Settings'
                ]);
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error('No se pudo guardar la configuración' . json_encode($setting->getErrors()),
                ["key" => "setting"]);
        }

        $this->set(compact('setting'));
    }

/**
* Edit method
*
* @param string|null $id Setting id.
* @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
* @throws \Cake\Network\Exception\NotFoundException When record not found.
*/
public function edit($id = null)
{
    $setting = $this->Settings->get($id, [
    'contain' => []
    ]);
    if ($this->request->is(['patch', 'post', 'put'])) {
        $setting = $this->Settings->patchEntity($setting,
            $this->request->getData());
        if ($setting->type == "file"){
            $file = $this->request->getData("value");
            if ($file) {
                $ext          = pathinfo($file['name'], PATHINFO_EXTENSION);
                $file['name'] = Text::uuid() . '.' . $ext;
                $uri          = 'files/settings/';
                if (AppController::uploadFile($file, $uri)) {
                    $setting->value = $uri . $file['name'];
                }
            } else {
                $this->Flash->error('Sin archivo adjuntado',
                    ["key" => "setting"]);
            }
        }
        if ($this->Settings->save($setting)) {
            $this->Flash->success(__('success_edit', [__('Setting')]), [
                'key' => 'Settings'
            ]);

            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('error_edit', [__('Setting'),
            json_encode($setting->getErrors())]), [
            'key' => 'Setting'
        ]);
    }
        $this->set(compact('setting'));
}

    /**
    * Delete method
    *
    * @param string|null $id Setting id.
    * @return \Cake\Http\Response|null Redirects to index.
    * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $setting = $this->Settings->get($id);
        if ($setting->type == "file"){
            AppController::deleteFile($setting->value);
        }
        if ($this->Settings->delete($setting)) {
            $this->Flash->success('Eliminado correctamente', [
                'key' => 'Settings'
            ]);
        } else {
            $this->Flash->error(__('The setting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
