<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * FrontEnd Controller
 */
class FrontEndController extends AppController
{

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow();
        $this->viewBuilder()->setLayout('frontend');
    }

    public function index(){}
}
