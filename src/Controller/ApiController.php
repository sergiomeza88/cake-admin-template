<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;

/**
 * Api Controller
 * @property \App\Model\Table\UsersTable $Users
 */
class ApiController extends AppController
{

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow();
        $this->Security->setConfig('unlockedActions',
            [
                'login',
            ]);
        $this->getEventManager()->off($this->Csrf);
        $this->autoRender = false;
    }

    /**
     * Inicio de sesion
     */
    public function login(){
        if ($this->request->is('POST')) {
            $email = $this->request->getData('email');
            $pass  = $this->request->getData('password');
            if ($email && $pass) {
                $this->loadModel('Users');
                if ($user = $this->Users->find('byEmail',
                    ['email' => $email])) {
                    $verify = (new DefaultPasswordHasher)
                        ->check($pass, $user['password']);
                    if ($verify) {
                        AppController::encode(['status' => 'success', 'data' => $user]);
                        $this->Auth->setUser($user->toArray());
                    } else {
                        AppController::encode(
                            [
                                'status'  => 'failure',
                                'message' => 'Password incorrecto, intentalo de nuevo',
                                'email'   => $email,
                            ]
                        );
                    }
                } else {
                    AppController::encode(
                        [
                            'status'  => 'failure',
                            'message' => 'No hay usuario registrado con el correo ' . $email,
                            'email'   => $email
                        ]
                    );
                }
            }
        }
    }
}
