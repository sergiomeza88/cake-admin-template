<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Rols']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    public function indexProfesor(){
        $this->paginate = [
            'contain' => ['UserMeta', 'Rols']
        ];
        $users = $this->paginate(
            $this->Users->find("all")
            ->where(["rol_id" => 3])
        );

        $this->set(compact('users'));
        $this->render('index');
    }

    public function indexStudent(){
        $this->paginate = [
            'contain' => ['UserMeta', 'Rols']
        ];
        $users = $this->paginate(
            $this->Users->find("all")
                        ->where(["rol_id" => 2])
        );

        $this->set(compact('users'));
        $this->render('index');
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Rols', 'UserMeta']
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('success_add', ['User']), [
                    'key' => 'Users'
                ]);
                if ($user_meta = $this->request->getData('user_meta')){
                    $userMetaObj = $this->Users->UserMeta->newEntity($user_meta);
                    $userMetaObj->user_id = $user->id;
                    if ($this->Users->UserMeta->save($userMetaObj)){
                        $this->Flash->success(__('success_add', ['UserMeta']), [
                            'key' => 'Users'
                        ]);
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('error_add', ['UserMeta', json_encode($user->getErrors())]), [
                            'key' => 'User'
                        ]);
                    }
                } else {
                    return $this->redirect(['action' => 'index']);
                }
            }

            $this->Flash->error(__('error_add', ['User', json_encode($user->getErrors())]), [
                'key' => 'User'
            ]);
        }
        $rols = $this->Users->Rols->find('list', ['limit' => 200]);
        $this->set(compact('user', 'rols'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['UserMeta']
        ]);
        $user_meta = $user->user_meta[0];
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $rols = $this->Users->Rols->find('list', ['limit' => 200]);
        $this->set(compact('user', 'rols', 'user_meta'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //Iniciar sesion
    public function login()
    {
        $this->viewBuilder()->enableAutoLayout(false);
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $user['type'] = 'admin';
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('auth_error'), [
                    'key' => 'error'
                ]);
            }
        }
    }

    //Salir
    public function logout(){
        return $this->redirect($this->Auth->logout());
    }
}
