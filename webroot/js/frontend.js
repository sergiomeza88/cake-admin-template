$(document).ready(function () {
    // $('#dataTable').DataTable({
    //     "language": {
    //         "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
    //     }
    // });

    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : { allow_single_deselect: true },
        '.chosen-select-no-single' : { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, no encontramos lo que buscabas' },
        '.chosen-select-rtl'       : { rtl: true },
        '.chosen-select-width'     : { width: '95%' }
    };
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $('.multiple-select-add').tokenize2({
        tokensAllowCustom: true,
        placeholder      : "Agregar...",
    });
});
