{
    "results"
:
    [{
        "id"  : "0010 - Asalariados",
        "text": "0010 - Asalariados"
    },
     {
         "id"  : "0081 - Personas Naturales sin Actividad Econ\u00f3mica",
         "text": "0081 - Personas Naturales sin Actividad Econ\u00f3mica"
     },
     {
         "id"  : "0082 - Personas Naturales Subsidiadas por Terceros",
         "text": "0082 - Personas Naturales Subsidiadas por Terceros"
     },
     {
         "id"  : "0090 - Rentistas de Capital, solo para personas naturales.",
         "text": "0090 - Rentistas de Capital, solo para personas naturales."
     },
     {
         "id"  : "0111 - Cultivo de cereales (excepto arroz), legumbres y semillas oleaginosas.",
         "text": "0111 - Cultivo de cereales (excepto arroz), legumbres y semillas oleaginosas."
     },
     {
         "id"  : "0112 - Cultivo de arroz.",
         "text": "0112 - Cultivo de arroz."
     },
     {
         "id"  : "0113 - Cultivo de hortalizas, ra\u00edces y tub\u00e9rculos.",
         "text": "0113 - Cultivo de hortalizas, ra\u00edces y tub\u00e9rculos."
     },
     {
         "id"  : "0114 - Cultivo de tabaco.",
         "text": "0114 - Cultivo de tabaco."
     },
     {
         "id"  : "0115 - Cultivo de plantas textiles.",
         "text": "0115 - Cultivo de plantas textiles."
     },
     {
         "id"  : "0119 - Otros cultivos transitorios n.c.p.",
         "text": "0119 - Otros cultivos transitorios n.c.p."
     },
     {
         "id"  : "0121 - Cultivo de frutas tropicales y subtropicales.",
         "text": "0121 - Cultivo de frutas tropicales y subtropicales."
     },
     {
         "id"  : "0122 - Cultivo de pl\u00e1tano y banano.",
         "text": "0122 - Cultivo de pl\u00e1tano y banano."
     },
     {
         "id"  : "0123 - Cultivo de caf\u00e9.",
         "text": "0123 - Cultivo de caf\u00e9."
     },
     {
         "id"  : "0124 - Cultivo de ca\u00f1a de az\u00facar.",
         "text": "0124 - Cultivo de ca\u00f1a de az\u00facar."
     },
     {
         "id"  : "0125 - Cultivo de flor de corte.",
         "text": "0125 - Cultivo de flor de corte."
     },
     {
         "id"  : "0126 - Cultivo de palma para aceite (palma africana) y otros frutos oleaginosos.",
         "text": "0126 - Cultivo de palma para aceite (palma africana) y otros frutos oleaginosos."
     },
     {
         "id"  : "0127 - Cultivo de plantas con las que se preparan bebidas.",
         "text": "0127 - Cultivo de plantas con las que se preparan bebidas."
     },
     {
         "id"  : "0128 - Cultivo de especias y de plantas arom\u00e1ticas y medicinales.",
         "text": "0128 - Cultivo de especias y de plantas arom\u00e1ticas y medicinales."
     },
     {
         "id"  : "0129 - Otros cultivos permanentes n.c.p.",
         "text": "0129 - Otros cultivos permanentes n.c.p."
     },
     {
         "id"  : "0130 - Propagaci\u00f3n de plantas (actividades de los viveros, excepto viveros forestales).",
         "text": "0130 - Propagaci\u00f3n de plantas (actividades de los viveros, excepto viveros forestales)."
     },
     {
         "id"  : "0141 - Cr\u00eda de ganado bovino y bufalino.",
         "text": "0141 - Cr\u00eda de ganado bovino y bufalino."
     },
     {
         "id"  : "0142 - Cr\u00eda de caballos y otros equinos.",
         "text": "0142 - Cr\u00eda de caballos y otros equinos."
     },
     {
         "id"  : "0143 - Cr\u00eda de ovejas y cabras.",
         "text": "0143 - Cr\u00eda de ovejas y cabras."
     },
     {
         "id"  : "0144 - Cr\u00eda de ganado porcino.",
         "text": "0144 - Cr\u00eda de ganado porcino."
     },
     {
         "id"  : "0145 - Cr\u00eda de aves de corral.",
         "text": "0145 - Cr\u00eda de aves de corral."
     },
     {
         "id"  : "0149 - Cr\u00eda de otros animales n.c.p.",
         "text": "0149 - Cr\u00eda de otros animales n.c.p."
     },
     {
         "id"  : "0150 - Explotaci\u00f3n mixta (agr\u00edcola y pecuaria).",
         "text": "0150 - Explotaci\u00f3n mixta (agr\u00edcola y pecuaria)."
     },
     {
         "id"  : "0161 - Actividades de apoyo a la agricultura.",
         "text": "0161 - Actividades de apoyo a la agricultura."
     },
     {
         "id"  : "0162 - Actividades de apoyo a la ganader\u00eda.",
         "text": "0162 - Actividades de apoyo a la ganader\u00eda."
     },
     {
         "id"  : "0163 - Actividades posteriores a la cosecha.",
         "text": "0163 - Actividades posteriores a la cosecha."
     },
     {
         "id"  : "0164 - Tratamiento de semillas para propagaci\u00f3n.",
         "text": "0164 - Tratamiento de semillas para propagaci\u00f3n."
     },
     {
         "id"  : "0170 - Caza ordinaria y mediante trampas y actividades de servicios conexas.",
         "text": "0170 - Caza ordinaria y mediante trampas y actividades de servicios conexas."
     },
     {
         "id"  : "0210 - Silvicultura y otras actividades forestales.",
         "text": "0210 - Silvicultura y otras actividades forestales."
     },
     {
         "id"  : "0220 - Extracci\u00f3n de madera.",
         "text": "0220 - Extracci\u00f3n de madera."
     },
     {
         "id"  : "0230 - Recolecci\u00f3n de productos forestales diferentes a la madera.",
         "text": "0230 - Recolecci\u00f3n de productos forestales diferentes a la madera."
     },
     {
         "id"  : "0240 - Servicios de apoyo a la silvicultura.",
         "text": "0240 - Servicios de apoyo a la silvicultura."
     },
     {
         "id"  : "0311 - Pesca mar\u00edtima.",
         "text": "0311 - Pesca mar\u00edtima."
     },
     {
         "id"  : "0312 - Pesca de agua dulce.",
         "text": "0312 - Pesca de agua dulce."
     },
     {
         "id"  : "0321 - Acuicultura mar\u00edtima.",
         "text": "0321 - Acuicultura mar\u00edtima."
     },
     {
         "id"  : "0322 - Acuicultura de agua dulce.",
         "text": "0322 - Acuicultura de agua dulce."
     },
     {
         "id"  : "0510 - Extracci\u00f3n de hulla (carb\u00f3n de piedra).",
         "text": "0510 - Extracci\u00f3n de hulla (carb\u00f3n de piedra)."
     },
     {
         "id"  : "0520 - Extracci\u00f3n de carb\u00f3n lignito.",
         "text": "0520 - Extracci\u00f3n de carb\u00f3n lignito."
     },
     {
         "id"  : "0610 - Extracci\u00f3n de petr\u00f3leo crudo.",
         "text": "0610 - Extracci\u00f3n de petr\u00f3leo crudo."
     },
     {
         "id"  : "0620 - Extracci\u00f3n de gas natural.",
         "text": "0620 - Extracci\u00f3n de gas natural."
     },
     {
         "id"  : "0710 - Extracci\u00f3n de minerales de hierro.",
         "text": "0710 - Extracci\u00f3n de minerales de hierro."
     },
     {
         "id"  : "0721 - Extracci\u00f3n de minerales de uranio y de torio.",
         "text": "0721 - Extracci\u00f3n de minerales de uranio y de torio."
     },
     {
         "id"  : "0722 - Extracci\u00f3n de oro y otros metales preciosos.",
         "text": "0722 - Extracci\u00f3n de oro y otros metales preciosos."
     },
     {
         "id"  : "0723 - Extracci\u00f3n de minerales de n\u00edquel.",
         "text": "0723 - Extracci\u00f3n de minerales de n\u00edquel."
     },
     {
         "id"  : "0729 - Extracci\u00f3n de otros minerales metal\u00edferos no ferrosos n.c.p.",
         "text": "0729 - Extracci\u00f3n de otros minerales metal\u00edferos no ferrosos n.c.p."
     },
     {
         "id"  : "0811 - Extracci\u00f3n de piedra, arena, arcillas comunes, yeso y anhidrita.",
         "text": "0811 - Extracci\u00f3n de piedra, arena, arcillas comunes, yeso y anhidrita."
     },
     {
         "id"  : "0812 - Extracci\u00f3n de arcillas de uso industrial, caliza, caol\u00edn y bentonitas.",
         "text": "0812 - Extracci\u00f3n de arcillas de uso industrial, caliza, caol\u00edn y bentonitas."
     },
     {
         "id"  : "0820 - Extracci\u00f3n de esmeraldas, piedras preciosas y semipreciosas.",
         "text": "0820 - Extracci\u00f3n de esmeraldas, piedras preciosas y semipreciosas."
     },
     {
         "id"  : "0891 - Extracci\u00f3n de minerales para la fabricaci\u00f3n de abonos y productos qu\u00edmicos.",
         "text": "0891 - Extracci\u00f3n de minerales para la fabricaci\u00f3n de abonos y productos qu\u00edmicos."
     },
     {
         "id"  : "0892 - Extracci\u00f3n de halita (sal).",
         "text": "0892 - Extracci\u00f3n de halita (sal)."
     },
     {
         "id"  : "0899 - Extracci\u00f3n de otros minerales no met\u00e1licos n.c.p.",
         "text": "0899 - Extracci\u00f3n de otros minerales no met\u00e1licos n.c.p."
     },
     {
         "id"  : "0910 - Actividades de apoyo para la extracci\u00f3n de petr\u00f3leo y de gas natural.",
         "text": "0910 - Actividades de apoyo para la extracci\u00f3n de petr\u00f3leo y de gas natural."
     },
     {
         "id"  : "0990 - Actividades de apoyo para otras actividades de explotaci\u00f3n de minas y canteras.",
         "text": "0990 - Actividades de apoyo para otras actividades de explotaci\u00f3n de minas y canteras."
     },
     {
         "id"  : "1011 - Procesamiento y conservaci\u00f3n de carne y productos c\u00e1rnicos.",
         "text": "1011 - Procesamiento y conservaci\u00f3n de carne y productos c\u00e1rnicos."
     },
     {
         "id"  : "1012 - Procesamiento y conservaci\u00f3n de pescados, crust\u00e1ceos y moluscos.",
         "text": "1012 - Procesamiento y conservaci\u00f3n de pescados, crust\u00e1ceos y moluscos."
     },
     {
         "id"  : "1020 - Procesamiento y conservaci\u00f3n de frutas, legumbres, hortalizas y tub\u00e9rculos.",
         "text": "1020 - Procesamiento y conservaci\u00f3n de frutas, legumbres, hortalizas y tub\u00e9rculos."
     },
     {
         "id"  : "1030 - Elaboraci\u00f3n de aceites y grasas de origen vegetal y animal.",
         "text": "1030 - Elaboraci\u00f3n de aceites y grasas de origen vegetal y animal."
     },
     {
         "id"  : "1040 - Elaboraci\u00f3n de productos l\u00e1cteos.",
         "text": "1040 - Elaboraci\u00f3n de productos l\u00e1cteos."
     },
     {
         "id"  : "1051 - Elaboraci\u00f3n de productos de moliner\u00eda.",
         "text": "1051 - Elaboraci\u00f3n de productos de moliner\u00eda."
     },
     {
         "id"  : "1052 - Elaboraci\u00f3n de almidones y productos derivados del almid\u00f3n.",
         "text": "1052 - Elaboraci\u00f3n de almidones y productos derivados del almid\u00f3n."
     },
     {
         "id"  : "1061 - Trilla de caf\u00e9.",
         "text": "1061 - Trilla de caf\u00e9."
     },
     {
         "id"  : "1062 - Descafeinado, tosti\u00f3n y molienda del caf\u00e9.",
         "text": "1062 - Descafeinado, tosti\u00f3n y molienda del caf\u00e9."
     },
     {
         "id"  : "1063 - Otros derivados del caf\u00e9.",
         "text": "1063 - Otros derivados del caf\u00e9."
     },
     {
         "id"  : "1071 - Elaboraci\u00f3n y refinaci\u00f3n de az\u00facar.",
         "text": "1071 - Elaboraci\u00f3n y refinaci\u00f3n de az\u00facar."
     },
     {
         "id"  : "1072 - Elaboraci\u00f3n de panela.",
         "text": "1072 - Elaboraci\u00f3n de panela."
     },
     {
         "id"  : "1081 - Elaboraci\u00f3n de productos de panader\u00eda.",
         "text": "1081 - Elaboraci\u00f3n de productos de panader\u00eda."
     },
     {
         "id"  : "1082 - Elaboraci\u00f3n de cacao, chocolate y productos de confiter\u00eda.",
         "text": "1082 - Elaboraci\u00f3n de cacao, chocolate y productos de confiter\u00eda."
     },
     {
         "id"  : "1083 - Elaboraci\u00f3n de macarrones, fideos, alcuzcuz y productos farin\u00e1ceos similares.",
         "text": "1083 - Elaboraci\u00f3n de macarrones, fideos, alcuzcuz y productos farin\u00e1ceos similares."
     },
     {
         "id"  : "1084 - Elaboraci\u00f3n de comidas y platos preparados.",
         "text": "1084 - Elaboraci\u00f3n de comidas y platos preparados."
     },
     {
         "id"  : "1089 - Elaboraci\u00f3n de otros productos alimenticios n.c.p.",
         "text": "1089 - Elaboraci\u00f3n de otros productos alimenticios n.c.p."
     },
     {
         "id"  : "1090 - Elaboraci\u00f3n de alimentos preparados para animales.",
         "text": "1090 - Elaboraci\u00f3n de alimentos preparados para animales."
     },
     {
         "id"  : "1101 - Destilaci\u00f3n, rectificaci\u00f3n y mezcla de bebidas alcoh\u00f3licas.",
         "text": "1101 - Destilaci\u00f3n, rectificaci\u00f3n y mezcla de bebidas alcoh\u00f3licas."
     },
     {
         "id"  : "1102 - Elaboraci\u00f3n de bebidas fermentadas no destiladas.",
         "text": "1102 - Elaboraci\u00f3n de bebidas fermentadas no destiladas."
     },
     {
         "id"  : "1103 - Producci\u00f3n de malta, elaboraci\u00f3n de cervezas y otras bebidas malteadas.",
         "text": "1103 - Producci\u00f3n de malta, elaboraci\u00f3n de cervezas y otras bebidas malteadas."
     },
     {
         "id"  : "1104 - Elaboraci\u00f3n de bebidas no alcoh\u00f3licas, producci\u00f3n de aguas minerales y de otras aguas embotelladas.",
         "text": "1104 - Elaboraci\u00f3n de bebidas no alcoh\u00f3licas, producci\u00f3n de aguas minerales y de otras aguas embotelladas."
     },
     {
         "id"  : "1200 - Elaboraci\u00f3n de productos de tabaco.",
         "text": "1200 - Elaboraci\u00f3n de productos de tabaco."
     },
     {
         "id"  : "1311 - Preparaci\u00f3n e hilatura de fibras textiles.",
         "text": "1311 - Preparaci\u00f3n e hilatura de fibras textiles."
     },
     {
         "id"  : "1312 - Tejedur\u00eda de productos textiles.",
         "text": "1312 - Tejedur\u00eda de productos textiles."
     },
     {
         "id"  : "1313 - Acabado de productos textiles.",
         "text": "1313 - Acabado de productos textiles."
     },
     {
         "id"  : "1391 - Fabricaci\u00f3n de tejidos de punto y ganchillo.",
         "text": "1391 - Fabricaci\u00f3n de tejidos de punto y ganchillo."
     },
     {
         "id"  : "1392 - Confecci\u00f3n de art\u00edculos con materiales textiles, excepto prendas de vestir.",
         "text": "1392 - Confecci\u00f3n de art\u00edculos con materiales textiles, excepto prendas de vestir."
     },
     {
         "id"  : "1393 - Fabricaci\u00f3n de tapetes y alfombras para pisos.",
         "text": "1393 - Fabricaci\u00f3n de tapetes y alfombras para pisos."
     },
     {
         "id"  : "1394 - Fabricaci\u00f3n de cuerdas, cordeles, cables, bramantes y redes.",
         "text": "1394 - Fabricaci\u00f3n de cuerdas, cordeles, cables, bramantes y redes."
     },
     {
         "id"  : "1399 - Fabricaci\u00f3n de otros art\u00edculos textiles n.c.p.",
         "text": "1399 - Fabricaci\u00f3n de otros art\u00edculos textiles n.c.p."
     },
     {
         "id"  : "1410 - Confecci\u00f3n de prendas de vestir, excepto prendas de piel.",
         "text": "1410 - Confecci\u00f3n de prendas de vestir, excepto prendas de piel."
     },
     {
         "id"  : "1420 - Fabricaci\u00f3n de art\u00edculos de piel.",
         "text": "1420 - Fabricaci\u00f3n de art\u00edculos de piel."
     },
     {
         "id"  : "1430 - Fabricaci\u00f3n de art\u00edculos de punto y ganchillo.",
         "text": "1430 - Fabricaci\u00f3n de art\u00edculos de punto y ganchillo."
     },
     {
         "id"  : "1511 - Curtido y recurtido de cueros; recurtido y te\u00f1ido de pieles.",
         "text": "1511 - Curtido y recurtido de cueros; recurtido y te\u00f1ido de pieles."
     },
     {
         "id"  : "1512 - Fabricaci\u00f3n de art\u00edculos de viaje, bolsos de mano y art\u00edculos similares elaborados en cuero, y fabricaci\u00f3n de art\u00edculos de talabarter\u00eda y guarnicioner\u00eda.",
         "text": "1512 - Fabricaci\u00f3n de art\u00edculos de viaje, bolsos de mano y art\u00edculos similares elaborados en cuero, y fabricaci\u00f3n de art\u00edculos de talabarter\u00eda y guarnicioner\u00eda."
     },
     {
         "id"  : "1513 - Fabricaci\u00f3n de art\u00edculos de viaje, bolsos de mano y art\u00edculos similares; art\u00edculos de talabarter\u00eda y guarnicioner\u00eda elaborados en otros materiales.",
         "text": "1513 - Fabricaci\u00f3n de art\u00edculos de viaje, bolsos de mano y art\u00edculos similares; art\u00edculos de talabarter\u00eda y guarnicioner\u00eda elaborados en otros materiales."
     },
     {
         "id"  : "1521 - Fabricaci\u00f3n de calzado de cuero y piel, con cualquier tipo de suela.",
         "text": "1521 - Fabricaci\u00f3n de calzado de cuero y piel, con cualquier tipo de suela."
     },
     {
         "id"  : "1522 - Fabricaci\u00f3n de otros tipos de calzado, excepto calzado de cuero y piel.",
         "text": "1522 - Fabricaci\u00f3n de otros tipos de calzado, excepto calzado de cuero y piel."
     },
     {
         "id"  : "1523 - Fabricaci\u00f3n de partes del calzado.",
         "text": "1523 - Fabricaci\u00f3n de partes del calzado."
     },
     {
         "id"  : "1610 - Aserrado, acepillado e impregnaci\u00f3n de la madera.",
         "text": "1610 - Aserrado, acepillado e impregnaci\u00f3n de la madera."
     },
     {
         "id"  : "1620 - Fabricaci\u00f3n de hojas de madera para enchapado; fabricaci\u00f3n de tableros contrachapados, tableros laminados, tableros de part\u00edculas y otros tableros y paneles.",
         "text": "1620 - Fabricaci\u00f3n de hojas de madera para enchapado; fabricaci\u00f3n de tableros contrachapados, tableros laminados, tableros de part\u00edculas y otros tableros y paneles."
     },
     {
         "id"  : "1630 - Fabricaci\u00f3n de partes y piezas de madera, de carpinter\u00eda y ebanister\u00eda para la construcci\u00f3n.",
         "text": "1630 - Fabricaci\u00f3n de partes y piezas de madera, de carpinter\u00eda y ebanister\u00eda para la construcci\u00f3n."
     },
     {
         "id"  : "1640 - Fabricaci\u00f3n de recipientes de madera.",
         "text": "1640 - Fabricaci\u00f3n de recipientes de madera."
     },
     {
         "id"  : "1690 - Fabricaci\u00f3n de otros productos de madera; fabricaci\u00f3n de art\u00edculos de corcho, cester\u00eda y esparter\u00eda.",
         "text": "1690 - Fabricaci\u00f3n de otros productos de madera; fabricaci\u00f3n de art\u00edculos de corcho, cester\u00eda y esparter\u00eda."
     },
     {
         "id"  : "1701 - Fabricaci\u00f3n de pulpas (pastas) celul\u00f3sicas; papel y cart\u00f3n.",
         "text": "1701 - Fabricaci\u00f3n de pulpas (pastas) celul\u00f3sicas; papel y cart\u00f3n."
     },
     {
         "id"  : "1702 - Fabricaci\u00f3n de papel y cart\u00f3n ondulado (corrugado); fabricaci\u00f3n de envases, empaques y de embalajes de papel y cart\u00f3n.",
         "text": "1702 - Fabricaci\u00f3n de papel y cart\u00f3n ondulado (corrugado); fabricaci\u00f3n de envases, empaques y de embalajes de papel y cart\u00f3n."
     },
     {
         "id"  : "1709 - Fabricaci\u00f3n de otros art\u00edculos de papel y cart\u00f3n.",
         "text": "1709 - Fabricaci\u00f3n de otros art\u00edculos de papel y cart\u00f3n."
     },
     {
         "id"  : "1811 - Actividades de impresi\u00f3n.",
         "text": "1811 - Actividades de impresi\u00f3n."
     },
     {
         "id"  : "1812 - Actividades de servicios relacionados con la impresi\u00f3n.",
         "text": "1812 - Actividades de servicios relacionados con la impresi\u00f3n."
     },
     {
         "id"  : "1820 - Producci\u00f3n de copias a partir de grabaciones originales.",
         "text": "1820 - Producci\u00f3n de copias a partir de grabaciones originales."
     },
     {
         "id"  : "1910 - Fabricaci\u00f3n de productos de hornos de coque.",
         "text": "1910 - Fabricaci\u00f3n de productos de hornos de coque."
     },
     {
         "id"  : "1921 - Fabricaci\u00f3n de productos de la refinaci\u00f3n del petr\u00f3leo.",
         "text": "1921 - Fabricaci\u00f3n de productos de la refinaci\u00f3n del petr\u00f3leo."
     },
     {
         "id"  : "1922 - Actividad de mezcla de combustibles.",
         "text": "1922 - Actividad de mezcla de combustibles."
     },
     {
         "id"  : "2011 - Fabricaci\u00f3n de sustancias y productos qu\u00edmicos b\u00e1sicos.",
         "text": "2011 - Fabricaci\u00f3n de sustancias y productos qu\u00edmicos b\u00e1sicos."
     },
     {
         "id"  : "2012 - Fabricaci\u00f3n de abonos y compuestos inorg\u00e1nicos nitrogenados.",
         "text": "2012 - Fabricaci\u00f3n de abonos y compuestos inorg\u00e1nicos nitrogenados."
     },
     {
         "id"  : "2013 - Fabricaci\u00f3n de pl\u00e1sticos en formas primarias.",
         "text": "2013 - Fabricaci\u00f3n de pl\u00e1sticos en formas primarias."
     },
     {
         "id"  : "2014 - Fabricaci\u00f3n de caucho sint\u00e9tico en formas primarias.",
         "text": "2014 - Fabricaci\u00f3n de caucho sint\u00e9tico en formas primarias."
     },
     {
         "id"  : "2021 - Fabricaci\u00f3n de plaguicidas y otros productos qu\u00edmicos de uso agropecuario.",
         "text": "2021 - Fabricaci\u00f3n de plaguicidas y otros productos qu\u00edmicos de uso agropecuario."
     },
     {
         "id"  : "2022 - Fabricaci\u00f3n de pinturas, barnices y revestimientos similares, tintas para impresi\u00f3n y masillas.",
         "text": "2022 - Fabricaci\u00f3n de pinturas, barnices y revestimientos similares, tintas para impresi\u00f3n y masillas."
     },
     {
         "id"  : "2023 - Fabricaci\u00f3n de jabones y detergentes, preparados para limpiar y pulir; perfumes y preparados de tocador.",
         "text": "2023 - Fabricaci\u00f3n de jabones y detergentes, preparados para limpiar y pulir; perfumes y preparados de tocador."
     },
     {
         "id"  : "2029 - Fabricaci\u00f3n de otros productos qu\u00edmicos n.c.p.",
         "text": "2029 - Fabricaci\u00f3n de otros productos qu\u00edmicos n.c.p."
     },
     {
         "id"  : "2030 - Fabricaci\u00f3n de fibras sint\u00e9ticas y artificiales.",
         "text": "2030 - Fabricaci\u00f3n de fibras sint\u00e9ticas y artificiales."
     },
     {
         "id"  : "2100 - Fabricaci\u00f3n de productos farmac\u00e9uticos, sustancias qu\u00edmicas medicinales y productos bot\u00e1nicos de uso farmac\u00e9utico.",
         "text": "2100 - Fabricaci\u00f3n de productos farmac\u00e9uticos, sustancias qu\u00edmicas medicinales y productos bot\u00e1nicos de uso farmac\u00e9utico."
     },
     {
         "id"  : "2211 - Fabricaci\u00f3n de llantas y neum\u00e1ticos de caucho",
         "text": "2211 - Fabricaci\u00f3n de llantas y neum\u00e1ticos de caucho"
     },
     {
         "id"  : "2212 - Reencauche de llantas usadas",
         "text": "2212 - Reencauche de llantas usadas"
     },
     {
         "id"  : "2219 - Fabricaci\u00f3n de formas b\u00e1sicas de caucho y otros productos de caucho n.c.p.",
         "text": "2219 - Fabricaci\u00f3n de formas b\u00e1sicas de caucho y otros productos de caucho n.c.p."
     },
     {
         "id"  : "2221 - Fabricaci\u00f3n de formas b\u00e1sicas de pl\u00e1stico.",
         "text": "2221 - Fabricaci\u00f3n de formas b\u00e1sicas de pl\u00e1stico."
     },
     {
         "id"  : "2229 - Fabricaci\u00f3n de art\u00edculos de pl\u00e1stico n.c.p.",
         "text": "2229 - Fabricaci\u00f3n de art\u00edculos de pl\u00e1stico n.c.p."
     },
     {
         "id"  : "2310 - Fabricaci\u00f3n de vidrio y productos de vidrio.",
         "text": "2310 - Fabricaci\u00f3n de vidrio y productos de vidrio."
     },
     {
         "id"  : "2391 - Fabricaci\u00f3n de productos refractarios.",
         "text": "2391 - Fabricaci\u00f3n de productos refractarios."
     },
     {
         "id"  : "2392 - Fabricaci\u00f3n de materiales de arcilla para la construcci\u00f3n.",
         "text": "2392 - Fabricaci\u00f3n de materiales de arcilla para la construcci\u00f3n."
     },
     {
         "id"  : "2393 - Fabricaci\u00f3n de otros productos de cer\u00e1mica y porcelana.",
         "text": "2393 - Fabricaci\u00f3n de otros productos de cer\u00e1mica y porcelana."
     },
     {
         "id"  : "2394 - Fabricaci\u00f3n de cemento, cal y yeso.",
         "text": "2394 - Fabricaci\u00f3n de cemento, cal y yeso."
     },
     {
         "id"  : "2395 - Fabricaci\u00f3n de art\u00edculos de hormig\u00f3n, cemento y yeso.",
         "text": "2395 - Fabricaci\u00f3n de art\u00edculos de hormig\u00f3n, cemento y yeso."
     },
     {
         "id"  : "2396 - Corte, tallado y acabado de la piedra.",
         "text": "2396 - Corte, tallado y acabado de la piedra."
     },
     {
         "id"  : "2399 - Fabricaci\u00f3n de otros productos minerales no met\u00e1licos n.c.p.",
         "text": "2399 - Fabricaci\u00f3n de otros productos minerales no met\u00e1licos n.c.p."
     },
     {
         "id"  : "2410 - Industrias b\u00e1sicas de hierro y de acero.",
         "text": "2410 - Industrias b\u00e1sicas de hierro y de acero."
     },
     {
         "id"  : "2421 - Industrias b\u00e1sicas de metales preciosos.",
         "text": "2421 - Industrias b\u00e1sicas de metales preciosos."
     },
     {
         "id"  : "2429 - Industrias b\u00e1sicas de otros metales no ferrosos.",
         "text": "2429 - Industrias b\u00e1sicas de otros metales no ferrosos."
     },
     {
         "id"  : "2431 - Fundici\u00f3n de hierro y de acero.",
         "text": "2431 - Fundici\u00f3n de hierro y de acero."
     },
     {
         "id"  : "2432 - Fundici\u00f3n de metales no ferrosos.",
         "text": "2432 - Fundici\u00f3n de metales no ferrosos."
     },
     {
         "id"  : "2511 - Fabricaci\u00f3n de productos met\u00e1licos para uso estructural.",
         "text": "2511 - Fabricaci\u00f3n de productos met\u00e1licos para uso estructural."
     },
     {
         "id"  : "2512 - Fabricaci\u00f3n de tanques, dep\u00f3sitos y recipientes de metal, excepto los utilizados para el envase o transporte de mercanc\u00edas.",
         "text": "2512 - Fabricaci\u00f3n de tanques, dep\u00f3sitos y recipientes de metal, excepto los utilizados para el envase o transporte de mercanc\u00edas."
     },
     {
         "id"  : "2513 - Fabricaci\u00f3n de generadores de vapor, excepto calderas de agua caliente para calefacci\u00f3n central.",
         "text": "2513 - Fabricaci\u00f3n de generadores de vapor, excepto calderas de agua caliente para calefacci\u00f3n central."
     },
     {
         "id"  : "2520 - Fabricaci\u00f3n de armas y municiones.",
         "text": "2520 - Fabricaci\u00f3n de armas y municiones."
     },
     {
         "id"  : "2591 - Forja, prensado, estampado y laminado de metal; pulvimetalurgia.",
         "text": "2591 - Forja, prensado, estampado y laminado de metal; pulvimetalurgia."
     },
     {
         "id"  : "2592 - Tratamiento y revestimiento de metales; mecanizado.",
         "text": "2592 - Tratamiento y revestimiento de metales; mecanizado."
     },
     {
         "id"  : "2593 - Fabricaci\u00f3n de art\u00edculos de cuchiller\u00eda, herramientas de mano y art\u00edculos de ferreter\u00eda.",
         "text": "2593 - Fabricaci\u00f3n de art\u00edculos de cuchiller\u00eda, herramientas de mano y art\u00edculos de ferreter\u00eda."
     },
     {
         "id"  : "2599 - Fabricaci\u00f3n de otros productos elaborados de metal n.c.p.",
         "text": "2599 - Fabricaci\u00f3n de otros productos elaborados de metal n.c.p."
     },
     {
         "id"  : "2610 - Fabricaci\u00f3n de componentes y tableros electr\u00f3nicos.",
         "text": "2610 - Fabricaci\u00f3n de componentes y tableros electr\u00f3nicos."
     },
     {
         "id"  : "2620 - Fabricaci\u00f3n de computadoras y de equipo perif\u00e9rico.",
         "text": "2620 - Fabricaci\u00f3n de computadoras y de equipo perif\u00e9rico."
     },
     {
         "id"  : "2630 - Fabricaci\u00f3n de equipos de comunicaci\u00f3n.",
         "text": "2630 - Fabricaci\u00f3n de equipos de comunicaci\u00f3n."
     },
     {
         "id"  : "2640 - Fabricaci\u00f3n de aparatos electr\u00f3nicos de consumo.",
         "text": "2640 - Fabricaci\u00f3n de aparatos electr\u00f3nicos de consumo."
     },
     {
         "id"  : "2651 - Fabricaci\u00f3n de equipo de medici\u00f3n, prueba, navegaci\u00f3n y control.",
         "text": "2651 - Fabricaci\u00f3n de equipo de medici\u00f3n, prueba, navegaci\u00f3n y control."
     },
     {
         "id"  : "2652 - Fabricaci\u00f3n de relojes.",
         "text": "2652 - Fabricaci\u00f3n de relojes."
     },
     {
         "id"  : "2660 - Fabricaci\u00f3n de equipo de irradiaci\u00f3n y equipo electr\u00f3nico de uso m\u00e9dico y terap\u00e9utico.",
         "text": "2660 - Fabricaci\u00f3n de equipo de irradiaci\u00f3n y equipo electr\u00f3nico de uso m\u00e9dico y terap\u00e9utico."
     },
     {
         "id"  : "2670 - Fabricaci\u00f3n de instrumentos \u00f3pticos y equipo fotogr\u00e1fico.",
         "text": "2670 - Fabricaci\u00f3n de instrumentos \u00f3pticos y equipo fotogr\u00e1fico."
     },
     {
         "id"  : "2680 - Fabricaci\u00f3n de medios magn\u00e9ticos y \u00f3pticos para almacenamiento de datos.",
         "text": "2680 - Fabricaci\u00f3n de medios magn\u00e9ticos y \u00f3pticos para almacenamiento de datos."
     },
     {
         "id"  : "2711 - Fabricaci\u00f3n de motores, generadores y transformadores el\u00e9ctricos.",
         "text": "2711 - Fabricaci\u00f3n de motores, generadores y transformadores el\u00e9ctricos."
     },
     {
         "id"  : "2712 - Fabricaci\u00f3n de aparatos de distribuci\u00f3n y control de la energ\u00eda el\u00e9ctrica.",
         "text": "2712 - Fabricaci\u00f3n de aparatos de distribuci\u00f3n y control de la energ\u00eda el\u00e9ctrica."
     },
     {
         "id"  : "2720 - Fabricaci\u00f3n de pilas, bater\u00edas y acumuladores el\u00e9ctricos.",
         "text": "2720 - Fabricaci\u00f3n de pilas, bater\u00edas y acumuladores el\u00e9ctricos."
     },
     {
         "id"  : "2731 - Fabricaci\u00f3n de hilos y cables el\u00e9ctricos y de fibra \u00f3ptica.",
         "text": "2731 - Fabricaci\u00f3n de hilos y cables el\u00e9ctricos y de fibra \u00f3ptica."
     },
     {
         "id"  : "2732 - Fabricaci\u00f3n de dispositivos de cableado.",
         "text": "2732 - Fabricaci\u00f3n de dispositivos de cableado."
     },
     {
         "id"  : "2740 - Fabricaci\u00f3n de equipos el\u00e9ctricos de iluminaci\u00f3n.",
         "text": "2740 - Fabricaci\u00f3n de equipos el\u00e9ctricos de iluminaci\u00f3n."
     },
     {
         "id"  : "2750 - Fabricaci\u00f3n de aparatos de uso dom\u00e9stico.",
         "text": "2750 - Fabricaci\u00f3n de aparatos de uso dom\u00e9stico."
     },
     {
         "id"  : "2790 - Fabricaci\u00f3n de otros tipos de equipo el\u00e9ctrico n.c.p.",
         "text": "2790 - Fabricaci\u00f3n de otros tipos de equipo el\u00e9ctrico n.c.p."
     },
     {
         "id"  : "2811 - Fabricaci\u00f3n de motores, turbinas, y partes para motores de combusti\u00f3n interna.",
         "text": "2811 - Fabricaci\u00f3n de motores, turbinas, y partes para motores de combusti\u00f3n interna."
     },
     {
         "id"  : "2812 - Fabricaci\u00f3n de equipos de potencia hidr\u00e1ulica y neum\u00e1tica.",
         "text": "2812 - Fabricaci\u00f3n de equipos de potencia hidr\u00e1ulica y neum\u00e1tica."
     },
     {
         "id"  : "2813 - Fabricaci\u00f3n de otras bombas, compresores, grifos y v\u00e1lvulas.",
         "text": "2813 - Fabricaci\u00f3n de otras bombas, compresores, grifos y v\u00e1lvulas."
     },
     {
         "id"  : "2814 - Fabricaci\u00f3n de cojinetes, engranajes, trenes de engranajes y piezas de transmisi\u00f3n.",
         "text": "2814 - Fabricaci\u00f3n de cojinetes, engranajes, trenes de engranajes y piezas de transmisi\u00f3n."
     },
     {
         "id"  : "2815 - Fabricaci\u00f3n de hornos, hogares y quemadores industriales.",
         "text": "2815 - Fabricaci\u00f3n de hornos, hogares y quemadores industriales."
     },
     {
         "id"  : "2816 - Fabricaci\u00f3n de equipo de elevaci\u00f3n y manipulaci\u00f3n.",
         "text": "2816 - Fabricaci\u00f3n de equipo de elevaci\u00f3n y manipulaci\u00f3n."
     },
     {
         "id"  : "2817 - Fabricaci\u00f3n de maquinaria y equipo de oficina (excepto computadoras y equipo perif\u00e9rico).",
         "text": "2817 - Fabricaci\u00f3n de maquinaria y equipo de oficina (excepto computadoras y equipo perif\u00e9rico)."
     },
     {
         "id"  : "2818 - Fabricaci\u00f3n de herramientas manuales con motor.",
         "text": "2818 - Fabricaci\u00f3n de herramientas manuales con motor."
     },
     {
         "id"  : "2819 - Fabricaci\u00f3n de otros tipos de maquinaria y equipo de uso general n.c.p.",
         "text": "2819 - Fabricaci\u00f3n de otros tipos de maquinaria y equipo de uso general n.c.p."
     },
     {
         "id"  : "2821 - Fabricaci\u00f3n de maquinaria agropecuaria y forestal.",
         "text": "2821 - Fabricaci\u00f3n de maquinaria agropecuaria y forestal."
     },
     {
         "id"  : "2822 - Fabricaci\u00f3n de m\u00e1quinas formadoras de metal y de m\u00e1quinas herramienta.",
         "text": "2822 - Fabricaci\u00f3n de m\u00e1quinas formadoras de metal y de m\u00e1quinas herramienta."
     },
     {
         "id"  : "2823 - Fabricaci\u00f3n de maquinaria para la metalurgia.",
         "text": "2823 - Fabricaci\u00f3n de maquinaria para la metalurgia."
     },
     {
         "id"  : "2824 - Fabricaci\u00f3n de maquinaria para explotaci\u00f3n de minas y canteras y para obras de construcci\u00f3n.",
         "text": "2824 - Fabricaci\u00f3n de maquinaria para explotaci\u00f3n de minas y canteras y para obras de construcci\u00f3n."
     },
     {
         "id"  : "2825 - Fabricaci\u00f3n de maquinaria para la elaboraci\u00f3n de alimentos, bebidas y tabaco.",
         "text": "2825 - Fabricaci\u00f3n de maquinaria para la elaboraci\u00f3n de alimentos, bebidas y tabaco."
     },
     {
         "id"  : "2826 - Fabricaci\u00f3n de maquinaria para la elaboraci\u00f3n de productos textiles, prendas de vestir y cueros.",
         "text": "2826 - Fabricaci\u00f3n de maquinaria para la elaboraci\u00f3n de productos textiles, prendas de vestir y cueros."
     },
     {
         "id"  : "2829 - Fabricaci\u00f3n de otros tipos de maquinaria y equipo de uso especial n.c.p.",
         "text": "2829 - Fabricaci\u00f3n de otros tipos de maquinaria y equipo de uso especial n.c.p."
     },
     {
         "id"  : "2910 - Fabricaci\u00f3n de veh\u00edculos automotores y sus motores.",
         "text": "2910 - Fabricaci\u00f3n de veh\u00edculos automotores y sus motores."
     },
     {
         "id"  : "2920 - Fabricaci\u00f3n de carrocer\u00edas para veh\u00edculos automotores; fabricaci\u00f3n de remolques y semirremolques.",
         "text": "2920 - Fabricaci\u00f3n de carrocer\u00edas para veh\u00edculos automotores; fabricaci\u00f3n de remolques y semirremolques."
     },
     {
         "id"  : "2930 - Fabricaci\u00f3n de partes, piezas (autopartes) y accesorios (lujos) para veh\u00edculos automotores.",
         "text": "2930 - Fabricaci\u00f3n de partes, piezas (autopartes) y accesorios (lujos) para veh\u00edculos automotores."
     },
     {
         "id"  : "3011 - Construcci\u00f3n de barcos y de estructuras flotantes.",
         "text": "3011 - Construcci\u00f3n de barcos y de estructuras flotantes."
     },
     {
         "id"  : "3012 - Construcci\u00f3n de embarcaciones de recreo y deporte.",
         "text": "3012 - Construcci\u00f3n de embarcaciones de recreo y deporte."
     },
     {
         "id"  : "3020 - Fabricaci\u00f3n de locomotoras y de material rodante para ferrocarriles.",
         "text": "3020 - Fabricaci\u00f3n de locomotoras y de material rodante para ferrocarriles."
     },
     {
         "id"  : "3030 - Fabricaci\u00f3n de aeronaves, naves espaciales y de maquinaria conexa.",
         "text": "3030 - Fabricaci\u00f3n de aeronaves, naves espaciales y de maquinaria conexa."
     },
     {
         "id"  : "3040 - Fabricaci\u00f3n de veh\u00edculos militares de combate.",
         "text": "3040 - Fabricaci\u00f3n de veh\u00edculos militares de combate."
     },
     {
         "id"  : "3091 - Fabricaci\u00f3n de motocicletas.",
         "text": "3091 - Fabricaci\u00f3n de motocicletas."
     },
     {
         "id"  : "3092 - Fabricaci\u00f3n de bicicletas y de sillas de ruedas para personas con discapacidad.",
         "text": "3092 - Fabricaci\u00f3n de bicicletas y de sillas de ruedas para personas con discapacidad."
     },
     {
         "id"  : "3099 - Fabricaci\u00f3n de otros tipos de equipo de transporte n.c.p.",
         "text": "3099 - Fabricaci\u00f3n de otros tipos de equipo de transporte n.c.p."
     },
     {
         "id"  : "3110 - Fabricaci\u00f3n de muebles.",
         "text": "3110 - Fabricaci\u00f3n de muebles."
     },
     {
         "id"  : "3120 - Fabricaci\u00f3n de colchones y somieres.",
         "text": "3120 - Fabricaci\u00f3n de colchones y somieres."
     },
     {
         "id"  : "3210 - Fabricaci\u00f3n de joyas, bisuter\u00eda y art\u00edculos conexos.",
         "text": "3210 - Fabricaci\u00f3n de joyas, bisuter\u00eda y art\u00edculos conexos."
     },
     {
         "id"  : "3220 - Fabricaci\u00f3n de instrumentos musicales.",
         "text": "3220 - Fabricaci\u00f3n de instrumentos musicales."
     },
     {
         "id"  : "3230 - Fabricaci\u00f3n de art\u00edculos y equipo para la pr\u00e1ctica del deporte.",
         "text": "3230 - Fabricaci\u00f3n de art\u00edculos y equipo para la pr\u00e1ctica del deporte."
     },
     {
         "id"  : "3240 - Fabricaci\u00f3n de juegos, juguetes y rompecabezas.",
         "text": "3240 - Fabricaci\u00f3n de juegos, juguetes y rompecabezas."
     },
     {
         "id"  : "3250 - Fabricaci\u00f3n de instrumentos, aparatos y materiales m\u00e9dicos y odontol\u00f3gicos (incluido mobiliario).",
         "text": "3250 - Fabricaci\u00f3n de instrumentos, aparatos y materiales m\u00e9dicos y odontol\u00f3gicos (incluido mobiliario)."
     },
     {
         "id"  : "3290 - Otras industrias manufactureras n.c.p.",
         "text": "3290 - Otras industrias manufactureras n.c.p."
     },
     {
         "id"  : "3311 - Mantenimiento y reparaci\u00f3n especializado de productos elaborados en metal.",
         "text": "3311 - Mantenimiento y reparaci\u00f3n especializado de productos elaborados en metal."
     },
     {
         "id"  : "3312 - Mantenimiento y reparaci\u00f3n especializado de maquinaria y equipo.",
         "text": "3312 - Mantenimiento y reparaci\u00f3n especializado de maquinaria y equipo."
     },
     {
         "id"  : "3313 - Mantenimiento y reparaci\u00f3n especializado de equipo electr\u00f3nico y \u00f3ptico.",
         "text": "3313 - Mantenimiento y reparaci\u00f3n especializado de equipo electr\u00f3nico y \u00f3ptico."
     },
     {
         "id"  : "3314 - Mantenimiento y reparaci\u00f3n especializado de equipo el\u00e9ctrico.",
         "text": "3314 - Mantenimiento y reparaci\u00f3n especializado de equipo el\u00e9ctrico."
     },
     {
         "id"  : "3315 - Mantenimiento y reparaci\u00f3n especializado de equipo de transporte, excepto los veh\u00edculos automotores, motocicletas y bicicletas.",
         "text": "3315 - Mantenimiento y reparaci\u00f3n especializado de equipo de transporte, excepto los veh\u00edculos automotores, motocicletas y bicicletas."
     },
     {
         "id"  : "3319 - Mantenimiento y reparaci\u00f3n de otros tipos de equipos y sus componentes n.c.p.",
         "text": "3319 - Mantenimiento y reparaci\u00f3n de otros tipos de equipos y sus componentes n.c.p."
     },
     {
         "id"  : "3320 - Instalaci\u00f3n especializada de maquinaria y equipo industrial.",
         "text": "3320 - Instalaci\u00f3n especializada de maquinaria y equipo industrial."
     },
     {
         "id"  : "3511 - Generaci\u00f3n de energ\u00eda el\u00e9ctrica.",
         "text": "3511 - Generaci\u00f3n de energ\u00eda el\u00e9ctrica."
     },
     {
         "id"  : "3512 - Transmisi\u00f3n de energ\u00eda el\u00e9ctrica.",
         "text": "3512 - Transmisi\u00f3n de energ\u00eda el\u00e9ctrica."
     },
     {
         "id"  : "3513 - Distribuci\u00f3n de energ\u00eda el\u00e9ctrica.",
         "text": "3513 - Distribuci\u00f3n de energ\u00eda el\u00e9ctrica."
     },
     {
         "id"  : "3514 - Comercializaci\u00f3n de energ\u00eda el\u00e9ctrica.",
         "text": "3514 - Comercializaci\u00f3n de energ\u00eda el\u00e9ctrica."
     },
     {
         "id"  : "3520 - Producci\u00f3n de gas; distribuci\u00f3n de combustibles gaseosos por tuber\u00edas.",
         "text": "3520 - Producci\u00f3n de gas; distribuci\u00f3n de combustibles gaseosos por tuber\u00edas."
     },
     {
         "id"  : "3530 - Suministro de vapor y aire acondicionado.",
         "text": "3530 - Suministro de vapor y aire acondicionado."
     },
     {
         "id"  : "3600 - Captaci\u00f3n, tratamiento y distribuci\u00f3n de agua.",
         "text": "3600 - Captaci\u00f3n, tratamiento y distribuci\u00f3n de agua."
     },
     {
         "id"  : "3700 - Evacuaci\u00f3n y tratamiento de aguas residuales.",
         "text": "3700 - Evacuaci\u00f3n y tratamiento de aguas residuales."
     },
     {
         "id"  : "3811 - Recolecci\u00f3n de desechos no peligrosos.",
         "text": "3811 - Recolecci\u00f3n de desechos no peligrosos."
     },
     {
         "id"  : "3812 - Recolecci\u00f3n de desechos peligrosos.",
         "text": "3812 - Recolecci\u00f3n de desechos peligrosos."
     },
     {
         "id"  : "3821 - Tratamiento y disposici\u00f3n de desechos no peligrosos.",
         "text": "3821 - Tratamiento y disposici\u00f3n de desechos no peligrosos."
     },
     {
         "id"  : "3822 - Tratamiento y disposici\u00f3n de desechos peligrosos.",
         "text": "3822 - Tratamiento y disposici\u00f3n de desechos peligrosos."
     },
     {
         "id"  : "3830 - Recuperaci\u00f3n de materiales.",
         "text": "3830 - Recuperaci\u00f3n de materiales."
     },
     {
         "id"  : "3900 - Actividades de saneamiento ambiental y otros servicios de gesti\u00f3n de desechos.",
         "text": "3900 - Actividades de saneamiento ambiental y otros servicios de gesti\u00f3n de desechos."
     },
     {
         "id"  : "4111 - Construcci\u00f3n de edificios residenciales.",
         "text": "4111 - Construcci\u00f3n de edificios residenciales."
     },
     {
         "id"  : "4112 - Construcci\u00f3n de edificios no residenciales.",
         "text": "4112 - Construcci\u00f3n de edificios no residenciales."
     },
     {
         "id"  : "4210 - Construcci\u00f3n de carreteras y v\u00edas de ferrocarril.",
         "text": "4210 - Construcci\u00f3n de carreteras y v\u00edas de ferrocarril."
     },
     {
         "id"  : "4220 - Construcci\u00f3n de proyectos de servicio p\u00fablico.",
         "text": "4220 - Construcci\u00f3n de proyectos de servicio p\u00fablico."
     },
     {
         "id"  : "4290 - Construcci\u00f3n de otras obras de ingenier\u00eda civil.",
         "text": "4290 - Construcci\u00f3n de otras obras de ingenier\u00eda civil."
     },
     {
         "id"  : "4311 - Demolici\u00f3n.",
         "text": "4311 - Demolici\u00f3n."
     },
     {
         "id"  : "4312 - Preparaci\u00f3n del terreno.",
         "text": "4312 - Preparaci\u00f3n del terreno."
     },
     {
         "id"  : "4321 - Instalaciones el\u00e9ctricas.",
         "text": "4321 - Instalaciones el\u00e9ctricas."
     },
     {
         "id"  : "4322 - Instalaciones de fontaner\u00eda, calefacci\u00f3n y aire acondicionado.",
         "text": "4322 - Instalaciones de fontaner\u00eda, calefacci\u00f3n y aire acondicionado."
     },
     {
         "id"  : "4329 - Otras instalaciones especializadas.",
         "text": "4329 - Otras instalaciones especializadas."
     },
     {
         "id"  : "4330 - Terminaci\u00f3n y acabado de edificios y obras de ingenier\u00eda civil.",
         "text": "4330 - Terminaci\u00f3n y acabado de edificios y obras de ingenier\u00eda civil."
     },
     {
         "id"  : "4390 - Otras actividades especializadas para la construcci\u00f3n de edificios y obras de ingenier\u00eda civil.",
         "text": "4390 - Otras actividades especializadas para la construcci\u00f3n de edificios y obras de ingenier\u00eda civil."
     },
     {
         "id"  : "4511 - Comercio de veh\u00edculos automotores nuevos.",
         "text": "4511 - Comercio de veh\u00edculos automotores nuevos."
     },
     {
         "id"  : "4512 - Comercio de veh\u00edculos automotores usados.",
         "text": "4512 - Comercio de veh\u00edculos automotores usados."
     },
     {
         "id"  : "4520 - Mantenimiento y reparaci\u00f3n de veh\u00edculos automotores.",
         "text": "4520 - Mantenimiento y reparaci\u00f3n de veh\u00edculos automotores."
     },
     {
         "id"  : "4530 - Comercio de partes, piezas (autopartes) y accesorios (lujos) para veh\u00edculos automotores.",
         "text": "4530 - Comercio de partes, piezas (autopartes) y accesorios (lujos) para veh\u00edculos automotores."
     },
     {
         "id"  : "4541 - Comercio de motocicletas y de sus partes, piezas y accesorios.",
         "text": "4541 - Comercio de motocicletas y de sus partes, piezas y accesorios."
     },
     {
         "id"  : "4542 - Mantenimiento y reparaci\u00f3n de motocicletas y de sus partes y piezas.",
         "text": "4542 - Mantenimiento y reparaci\u00f3n de motocicletas y de sus partes y piezas."
     },
     {
         "id"  : "4610 - Comercio al por mayor a cambio de una retribuci\u00f3n o por contrata.",
         "text": "4610 - Comercio al por mayor a cambio de una retribuci\u00f3n o por contrata."
     },
     {
         "id"  : "4620 - Comercio al por mayor de materias primas agropecuarias; animales vivos.",
         "text": "4620 - Comercio al por mayor de materias primas agropecuarias; animales vivos."
     },
     {
         "id"  : "4631 - Comercio al por mayor de productos alimenticios.",
         "text": "4631 - Comercio al por mayor de productos alimenticios."
     },
     {
         "id"  : "4632 - Comercio al por mayor de bebidas y tabaco.",
         "text": "4632 - Comercio al por mayor de bebidas y tabaco."
     },
     {
         "id"  : "4641 - Comercio al por mayor de productos textiles, productos confeccionados para uso dom\u00e9stico.",
         "text": "4641 - Comercio al por mayor de productos textiles, productos confeccionados para uso dom\u00e9stico."
     },
     {
         "id"  : "4642 - Comercio al por mayor de prendas de vestir.",
         "text": "4642 - Comercio al por mayor de prendas de vestir."
     },
     {
         "id"  : "4643 - Comercio al por mayor de calzado.",
         "text": "4643 - Comercio al por mayor de calzado."
     },
     {
         "id"  : "4644 - Comercio al por mayor de aparatos y equipo de uso dom\u00e9stico.",
         "text": "4644 - Comercio al por mayor de aparatos y equipo de uso dom\u00e9stico."
     },
     {
         "id"  : "4645 - Comercio al por mayor de productos farmac\u00e9uticos, medicinales, cosm\u00e9ticos y de tocador.",
         "text": "4645 - Comercio al por mayor de productos farmac\u00e9uticos, medicinales, cosm\u00e9ticos y de tocador."
     },
     {
         "id"  : "4649 - Comercio al por mayor de otros utensilios dom\u00e9sticos n.c.p.",
         "text": "4649 - Comercio al por mayor de otros utensilios dom\u00e9sticos n.c.p."
     },
     {
         "id"  : "4651 - Comercio al por mayor de computadores, equipo perif\u00e9rico y programas de inform\u00e1tica.",
         "text": "4651 - Comercio al por mayor de computadores, equipo perif\u00e9rico y programas de inform\u00e1tica."
     },
     {
         "id"  : "4652 - Comercio al por mayor de equipo, partes y piezas electr\u00f3nicos y de telecomunicaciones.",
         "text": "4652 - Comercio al por mayor de equipo, partes y piezas electr\u00f3nicos y de telecomunicaciones."
     },
     {
         "id"  : "4653 - Comercio al por mayor de maquinaria y equipo agropecuarios.",
         "text": "4653 - Comercio al por mayor de maquinaria y equipo agropecuarios."
     },
     {
         "id"  : "4659 - Comercio al por mayor de otros tipos de maquinaria y equipo n.c.p.",
         "text": "4659 - Comercio al por mayor de otros tipos de maquinaria y equipo n.c.p."
     },
     {
         "id"  : "4661 - Comercio al por mayor de combustibles s\u00f3lidos, l\u00edquidos, gaseosos y productos conexos.",
         "text": "4661 - Comercio al por mayor de combustibles s\u00f3lidos, l\u00edquidos, gaseosos y productos conexos."
     },
     {
         "id"  : "4662 - Comercio al por mayor de metales y productos metal\u00edferos.",
         "text": "4662 - Comercio al por mayor de metales y productos metal\u00edferos."
     },
     {
         "id"  : "4663 - Comercio al por mayor de materiales de construcci\u00f3n, art\u00edculos de ferreter\u00eda, pinturas, productos de vidrio, equipo y materiales de fontaner\u00eda y calefacci\u00f3n.",
         "text": "4663 - Comercio al por mayor de materiales de construcci\u00f3n, art\u00edculos de ferreter\u00eda, pinturas, productos de vidrio, equipo y materiales de fontaner\u00eda y calefacci\u00f3n."
     },
     {
         "id"  : "4664 - Comercio al por mayor de productos qu\u00edmicos b\u00e1sicos, cauchos y pl\u00e1sticos en formas primarias y productos qu\u00edmicos de uso agropecuario.",
         "text": "4664 - Comercio al por mayor de productos qu\u00edmicos b\u00e1sicos, cauchos y pl\u00e1sticos en formas primarias y productos qu\u00edmicos de uso agropecuario."
     },
     {
         "id"  : "4665 - Comercio al por mayor de desperdicios, desechos y chatarra.",
         "text": "4665 - Comercio al por mayor de desperdicios, desechos y chatarra."
     },
     {
         "id"  : "4669 - Comercio al por mayor de otros productos n.c.p.",
         "text": "4669 - Comercio al por mayor de otros productos n.c.p."
     },
     {
         "id"  : "4690 - Comercio al por mayor no especializado.",
         "text": "4690 - Comercio al por mayor no especializado."
     },
     {
         "id"  : "4711 - Comercio al por menor en establecimientos no especializados con surtido compuesto principalmente por alimentos, bebidas o tabaco.",
         "text": "4711 - Comercio al por menor en establecimientos no especializados con surtido compuesto principalmente por alimentos, bebidas o tabaco."
     },
     {
         "id"  : "4719 - Comercio al por menor en establecimientos no especializados, con surtido compuesto principalmente por productos diferentes de alimentos (v\u00edveres en general), bebidas y tabaco.",
         "text": "4719 - Comercio al por menor en establecimientos no especializados, con surtido compuesto principalmente por productos diferentes de alimentos (v\u00edveres en general), bebidas y tabaco."
     },
     {
         "id"  : "4721 - Comercio al por menor de productos agr\u00edcolas para el consumo en establecimientos especializados.",
         "text": "4721 - Comercio al por menor de productos agr\u00edcolas para el consumo en establecimientos especializados."
     },
     {
         "id"  : "4722 - Comercio al por menor de leche, productos l\u00e1cteos y huevos, en establecimientos especializados.",
         "text": "4722 - Comercio al por menor de leche, productos l\u00e1cteos y huevos, en establecimientos especializados."
     },
     {
         "id"  : "4723 - Comercio al por menor de carnes (incluye aves de corral), productos c\u00e1rnicos, pescados y productos de mar, en establecimientos especializados.",
         "text": "4723 - Comercio al por menor de carnes (incluye aves de corral), productos c\u00e1rnicos, pescados y productos de mar, en establecimientos especializados."
     },
     {
         "id"  : "4724 - Comercio al por menor de bebidas y productos del tabaco, en establecimientos especializados.",
         "text": "4724 - Comercio al por menor de bebidas y productos del tabaco, en establecimientos especializados."
     },
     {
         "id"  : "4729 - Comercio al por menor de otros productos alimenticios n.c.p., en establecimientos especializados.",
         "text": "4729 - Comercio al por menor de otros productos alimenticios n.c.p., en establecimientos especializados."
     },
     {
         "id"  : "4731 - Comercio al por menor de combustible para automotores.",
         "text": "4731 - Comercio al por menor de combustible para automotores."
     },
     {
         "id"  : "4732 - Comercio al por menor de lubricantes (aceites, grasas), aditivos y productos de limpieza para veh\u00edculos automotores.",
         "text": "4732 - Comercio al por menor de lubricantes (aceites, grasas), aditivos y productos de limpieza para veh\u00edculos automotores."
     },
     {
         "id"  : "4741 - Comercio al por menor de computadores, equipos perif\u00e9ricos, programas de inform\u00e1tica y equipos de telecomunicaciones en establecimientos especializados.",
         "text": "4741 - Comercio al por menor de computadores, equipos perif\u00e9ricos, programas de inform\u00e1tica y equipos de telecomunicaciones en establecimientos especializados."
     },
     {
         "id"  : "4742 - Comercio al por menor de equipos y aparatos de sonido y de video, en establecimientos especializados.",
         "text": "4742 - Comercio al por menor de equipos y aparatos de sonido y de video, en establecimientos especializados."
     },
     {
         "id"  : "4751 - Comercio al por menor de productos textiles en establecimientos especializados.",
         "text": "4751 - Comercio al por menor de productos textiles en establecimientos especializados."
     },
     {
         "id"  : "4752 - Comercio al por menor de art\u00edculos de ferreter\u00eda, pinturas y productos de vidrio en establecimientos especializados.",
         "text": "4752 - Comercio al por menor de art\u00edculos de ferreter\u00eda, pinturas y productos de vidrio en establecimientos especializados."
     },
     {
         "id"  : "4753 - Comercio al por menor de tapices, alfombras y cubrimientos para paredes y pisos en establecimientos especializados.",
         "text": "4753 - Comercio al por menor de tapices, alfombras y cubrimientos para paredes y pisos en establecimientos especializados."
     },
     {
         "id"  : "4754 - Comercio al por menor de electrodom\u00e9sticos y gasodom\u00e9sticos de uso dom\u00e9stico, muebles y equipos de iluminaci\u00f3n.",
         "text": "4754 - Comercio al por menor de electrodom\u00e9sticos y gasodom\u00e9sticos de uso dom\u00e9stico, muebles y equipos de iluminaci\u00f3n."
     },
     {
         "id"  : "4755 - Comercio al por menor de art\u00edculos y utensilios de uso dom\u00e9stico.",
         "text": "4755 - Comercio al por menor de art\u00edculos y utensilios de uso dom\u00e9stico."
     },
     {
         "id"  : "4759 - Comercio al por menor de otros art\u00edculos dom\u00e9sticos en establecimientos especializados.",
         "text": "4759 - Comercio al por menor de otros art\u00edculos dom\u00e9sticos en establecimientos especializados."
     },
     {
         "id"  : "4761 - Comercio al por menor de libros, peri\u00f3dicos, materiales y art\u00edculos de papeler\u00eda y escritorio, en establecimientos especializados.",
         "text": "4761 - Comercio al por menor de libros, peri\u00f3dicos, materiales y art\u00edculos de papeler\u00eda y escritorio, en establecimientos especializados."
     },
     {
         "id"  : "4762 - Comercio al por menor de art\u00edculos deportivos, en establecimientos especializados.",
         "text": "4762 - Comercio al por menor de art\u00edculos deportivos, en establecimientos especializados."
     },
     {
         "id"  : "4769 - Comercio al por menor de otros art\u00edculos culturales y de entretenimiento n.c.p. en establecimientos especializados.",
         "text": "4769 - Comercio al por menor de otros art\u00edculos culturales y de entretenimiento n.c.p. en establecimientos especializados."
     },
     {
         "id"  : "4771 - Comercio al por menor de prendas de vestir y sus accesorios (incluye art\u00edculos de piel) en establecimientos especializados.",
         "text": "4771 - Comercio al por menor de prendas de vestir y sus accesorios (incluye art\u00edculos de piel) en establecimientos especializados."
     },
     {
         "id"  : "4772 - Comercio al por menor de todo tipo de calzado y art\u00edculos de cuero y suced\u00e1neos del cuero en establecimientos especializados.",
         "text": "4772 - Comercio al por menor de todo tipo de calzado y art\u00edculos de cuero y suced\u00e1neos del cuero en establecimientos especializados."
     },
     {
         "id"  : "4773 - Comercio al por menor de productos farmac\u00e9uticos y medicinales, cosm\u00e9ticos y art\u00edculos de tocador en establecimientos especializados.",
         "text": "4773 - Comercio al por menor de productos farmac\u00e9uticos y medicinales, cosm\u00e9ticos y art\u00edculos de tocador en establecimientos especializados."
     },
     {
         "id"  : "4774 - Comercio al por menor de otros productos nuevos en establecimientos especializados.",
         "text": "4774 - Comercio al por menor de otros productos nuevos en establecimientos especializados."
     },
     {
         "id"  : "4775 - Comercio al por menor de art\u00edculos de segunda mano.",
         "text": "4775 - Comercio al por menor de art\u00edculos de segunda mano."
     },
     {
         "id"  : "4781 - Comercio al por menor de alimentos, bebidas y tabaco, en puestos de venta m\u00f3viles.",
         "text": "4781 - Comercio al por menor de alimentos, bebidas y tabaco, en puestos de venta m\u00f3viles."
     },
     {
         "id"  : "4782 - Comercio al por menor de productos textiles, prendas de vestir y calzado, en puestos de venta m\u00f3viles.",
         "text": "4782 - Comercio al por menor de productos textiles, prendas de vestir y calzado, en puestos de venta m\u00f3viles."
     },
     {
         "id"  : "4789 - Comercio al por menor de otros productos en puestos de venta m\u00f3viles.",
         "text": "4789 - Comercio al por menor de otros productos en puestos de venta m\u00f3viles."
     },
     {
         "id"  : "4791 - Comercio al por menor realizado a trav\u00e9s de internet.",
         "text": "4791 - Comercio al por menor realizado a trav\u00e9s de internet."
     },
     {
         "id"  : "4792 - Comercio al por menor realizado a trav\u00e9s de casas de venta o por correo.",
         "text": "4792 - Comercio al por menor realizado a trav\u00e9s de casas de venta o por correo."
     },
     {
         "id"  : "4799 - Otros tipos de comercio al por menor no realizado en establecimientos, puestos de venta o mercados.",
         "text": "4799 - Otros tipos de comercio al por menor no realizado en establecimientos, puestos de venta o mercados."
     },
     {
         "id"  : "4911 - Transporte f\u00e9rreo de pasajeros.",
         "text": "4911 - Transporte f\u00e9rreo de pasajeros."
     },
     {
         "id"  : "4912 - Transporte f\u00e9rreo de carga.",
         "text": "4912 - Transporte f\u00e9rreo de carga."
     },
     {
         "id"  : "4921 - Transporte de pasajeros.",
         "text": "4921 - Transporte de pasajeros."
     },
     {
         "id"  : "4922 - Transporte mixto.",
         "text": "4922 - Transporte mixto."
     },
     {
         "id"  : "4923 - Transporte de carga por carretera.",
         "text": "4923 - Transporte de carga por carretera."
     },
     {
         "id"  : "4930 - Transporte por tuber\u00edas.",
         "text": "4930 - Transporte por tuber\u00edas."
     },
     {
         "id"  : "5011 - Transporte de pasajeros mar\u00edtimo y de cabotaje.",
         "text": "5011 - Transporte de pasajeros mar\u00edtimo y de cabotaje."
     },
     {
         "id"  : "5012 - Transporte de carga mar\u00edtimo y de cabotaje.",
         "text": "5012 - Transporte de carga mar\u00edtimo y de cabotaje."
     },
     {
         "id"  : "5021 - Transporte fluvial de pasajeros.",
         "text": "5021 - Transporte fluvial de pasajeros."
     },
     {
         "id"  : "5022 - Transporte fluvial de carga.",
         "text": "5022 - Transporte fluvial de carga."
     },
     {
         "id"  : "5111 - Transporte a\u00e9reo nacional de pasajeros.",
         "text": "5111 - Transporte a\u00e9reo nacional de pasajeros."
     },
     {
         "id"  : "5112 - Transporte a\u00e9reo internacional de pasajeros.",
         "text": "5112 - Transporte a\u00e9reo internacional de pasajeros."
     },
     {
         "id"  : "5121 - Transporte a\u00e9reo nacional de carga.",
         "text": "5121 - Transporte a\u00e9reo nacional de carga."
     },
     {
         "id"  : "5122 - Transporte a\u00e9reo internacional de carga.",
         "text": "5122 - Transporte a\u00e9reo internacional de carga."
     },
     {
         "id"  : "5210 - Almacenamiento y dep\u00f3sito.",
         "text": "5210 - Almacenamiento y dep\u00f3sito."
     },
     {
         "id"  : "5221 - Actividades de estaciones, v\u00edas y servicios complementarios para el transporte terrestre.",
         "text": "5221 - Actividades de estaciones, v\u00edas y servicios complementarios para el transporte terrestre."
     },
     {
         "id"  : "5222 - Actividades de puertos y servicios complementarios para el transporte acu\u00e1tico.",
         "text": "5222 - Actividades de puertos y servicios complementarios para el transporte acu\u00e1tico."
     },
     {
         "id"  : "5223 - Actividades de aeropuertos, servicios de navegaci\u00f3n a\u00e9rea y dem\u00e1s actividades conexas al transporte a\u00e9reo.",
         "text": "5223 - Actividades de aeropuertos, servicios de navegaci\u00f3n a\u00e9rea y dem\u00e1s actividades conexas al transporte a\u00e9reo."
     },
     {
         "id"  : "5224 - Manipulaci\u00f3n de carga.",
         "text": "5224 - Manipulaci\u00f3n de carga."
     },
     {
         "id"  : "5229 - Otras actividades complementarias al transporte.",
         "text": "5229 - Otras actividades complementarias al transporte."
     },
     {
         "id"  : "5310 - Actividades postales nacionales.",
         "text": "5310 - Actividades postales nacionales."
     },
     {
         "id"  : "5320 - Actividades de mensajer\u00eda.",
         "text": "5320 - Actividades de mensajer\u00eda."
     },
     {
         "id"  : "5511 - Alojamiento en hoteles.",
         "text": "5511 - Alojamiento en hoteles."
     },
     {
         "id"  : "5512 - Alojamiento en apartahoteles.",
         "text": "5512 - Alojamiento en apartahoteles."
     },
     {
         "id"  : "5513 - Alojamiento en centros vacacionales.",
         "text": "5513 - Alojamiento en centros vacacionales."
     },
     {
         "id"  : "5514 - Alojamiento rural.",
         "text": "5514 - Alojamiento rural."
     },
     {
         "id"  : "5519 - Otros tipos de alojamientos para visitantes.",
         "text": "5519 - Otros tipos de alojamientos para visitantes."
     },
     {
         "id"  : "5520 - Actividades de zonas de camping y parques para veh\u00edculos recreacionales.",
         "text": "5520 - Actividades de zonas de camping y parques para veh\u00edculos recreacionales."
     },
     {
         "id"  : "5530 - Servicio por horas",
         "text": "5530 - Servicio por horas"
     },
     {
         "id"  : "5590 - Otros tipos de alojamiento n.c.p.",
         "text": "5590 - Otros tipos de alojamiento n.c.p."
     },
     {
         "id"  : "5611 - Expendio a la mesa de comidas preparadas.",
         "text": "5611 - Expendio a la mesa de comidas preparadas."
     },
     {
         "id"  : "5612 - Expendio por autoservicio de comidas preparadas.",
         "text": "5612 - Expendio por autoservicio de comidas preparadas."
     },
     {
         "id"  : "5613 - Expendio de comidas preparadas en cafeter\u00edas.",
         "text": "5613 - Expendio de comidas preparadas en cafeter\u00edas."
     },
     {
         "id"  : "5619 - Otros tipos de expendio de comidas preparadas n.c.p.",
         "text": "5619 - Otros tipos de expendio de comidas preparadas n.c.p."
     },
     {
         "id"  : "5621 - Catering para eventos.",
         "text": "5621 - Catering para eventos."
     },
     {
         "id"  : "5629 - Actividades de otros servicios de comidas.",
         "text": "5629 - Actividades de otros servicios de comidas."
     },
     {
         "id"  : "5630 - Expendio de bebidas alcoh\u00f3licas para el consumo dentro del establecimiento.",
         "text": "5630 - Expendio de bebidas alcoh\u00f3licas para el consumo dentro del establecimiento."
     },
     {
         "id"  : "5811 - Edici\u00f3n de libros.",
         "text": "5811 - Edici\u00f3n de libros."
     },
     {
         "id"  : "5812 - Edici\u00f3n de directorios y listas de correo.",
         "text": "5812 - Edici\u00f3n de directorios y listas de correo."
     },
     {
         "id"  : "5813 - Edici\u00f3n de peri\u00f3dicos, revistas y otras publicaciones peri\u00f3dicas.",
         "text": "5813 - Edici\u00f3n de peri\u00f3dicos, revistas y otras publicaciones peri\u00f3dicas."
     },
     {
         "id"  : "5819 - Otros trabajos de edici\u00f3n.",
         "text": "5819 - Otros trabajos de edici\u00f3n."
     },
     {
         "id"  : "5820 - Edici\u00f3n de programas de inform\u00e1tica (software).",
         "text": "5820 - Edici\u00f3n de programas de inform\u00e1tica (software)."
     },
     {
         "id"  : "5911 - Actividades de producci\u00f3n de pel\u00edculas cinematogr\u00e1ficas, videos, programas, anuncios y comerciales de televisi\u00f3n.",
         "text": "5911 - Actividades de producci\u00f3n de pel\u00edculas cinematogr\u00e1ficas, videos, programas, anuncios y comerciales de televisi\u00f3n."
     },
     {
         "id"  : "5912 - Actividades de posproducci\u00f3n de pel\u00edculas cinematogr\u00e1ficas, videos, programas, anuncios y comerciales de televisi\u00f3n.",
         "text": "5912 - Actividades de posproducci\u00f3n de pel\u00edculas cinematogr\u00e1ficas, videos, programas, anuncios y comerciales de televisi\u00f3n."
     },
     {
         "id"  : "5913 - Actividades de distribuci\u00f3n de pel\u00edculas cinematogr\u00e1ficas, videos, programas, anuncios y comerciales de televisi\u00f3n.",
         "text": "5913 - Actividades de distribuci\u00f3n de pel\u00edculas cinematogr\u00e1ficas, videos, programas, anuncios y comerciales de televisi\u00f3n."
     },
     {
         "id"  : "5914 - Actividades de exhibici\u00f3n de pel\u00edculas cinematogr\u00e1ficas y videos.",
         "text": "5914 - Actividades de exhibici\u00f3n de pel\u00edculas cinematogr\u00e1ficas y videos."
     },
     {
         "id"  : "5920 - Actividades de grabaci\u00f3n de sonido y edici\u00f3n de m\u00fasica.",
         "text": "5920 - Actividades de grabaci\u00f3n de sonido y edici\u00f3n de m\u00fasica."
     },
     {
         "id"  : "6010 - Actividades de programaci\u00f3n y transmisi\u00f3n en el servicio de radiodifusi\u00f3n sonora.",
         "text": "6010 - Actividades de programaci\u00f3n y transmisi\u00f3n en el servicio de radiodifusi\u00f3n sonora."
     },
     {
         "id"  : "6020 - Actividades de programaci\u00f3n y transmisi\u00f3n de televisi\u00f3n.",
         "text": "6020 - Actividades de programaci\u00f3n y transmisi\u00f3n de televisi\u00f3n."
     },
     {
         "id"  : "6110 - Actividades de telecomunicaciones al\u00e1mbricas.",
         "text": "6110 - Actividades de telecomunicaciones al\u00e1mbricas."
     },
     {
         "id"  : "6120 - Actividades de telecomunicaciones inal\u00e1mbricas.",
         "text": "6120 - Actividades de telecomunicaciones inal\u00e1mbricas."
     },
     {
         "id"  : "6130 - Actividades de telecomunicaci\u00f3n satelital.",
         "text": "6130 - Actividades de telecomunicaci\u00f3n satelital."
     },
     {
         "id"  : "6190 - Otras actividades de telecomunicaciones.",
         "text": "6190 - Otras actividades de telecomunicaciones."
     },
     {
         "id"  : "6201 - Actividades de desarrollo de sistemas inform\u00e1ticos (planificaci\u00f3n, an\u00e1lisis, dise\u00f1o, programaci\u00f3n, pruebas).",
         "text": "6201 - Actividades de desarrollo de sistemas inform\u00e1ticos (planificaci\u00f3n, an\u00e1lisis, dise\u00f1o, programaci\u00f3n, pruebas)."
     },
     {
         "id"  : "6202 - Actividades de consultor\u00eda inform\u00e1tica y actividades de administraci\u00f3n de instalaciones inform\u00e1ticas.",
         "text": "6202 - Actividades de consultor\u00eda inform\u00e1tica y actividades de administraci\u00f3n de instalaciones inform\u00e1ticas."
     },
     {
         "id"  : "6209 - Otras actividades de tecnolog\u00edas de informaci\u00f3n y actividades de servicios inform\u00e1ticos.",
         "text": "6209 - Otras actividades de tecnolog\u00edas de informaci\u00f3n y actividades de servicios inform\u00e1ticos."
     },
     {
         "id"  : "6311 - Procesamiento de datos, alojamiento (hosting) y actividades relacionadas.",
         "text": "6311 - Procesamiento de datos, alojamiento (hosting) y actividades relacionadas."
     },
     {
         "id"  : "6312 - Portales web.",
         "text": "6312 - Portales web."
     },
     {
         "id"  : "6391 - Actividades de agencias de noticias.",
         "text": "6391 - Actividades de agencias de noticias."
     },
     {
         "id"  : "6399 - Otras actividades de servicio de informaci\u00f3n n.c.p.",
         "text": "6399 - Otras actividades de servicio de informaci\u00f3n n.c.p."
     },
     {
         "id"  : "6411 - Banco Central.",
         "text": "6411 - Banco Central."
     },
     {
         "id"  : "6412 - Bancos comerciales.",
         "text": "6412 - Bancos comerciales."
     },
     {
         "id"  : "6421 - Actividades de las corporaciones financieras.",
         "text": "6421 - Actividades de las corporaciones financieras."
     },
     {
         "id"  : "6422 - Actividades de las compa\u00f1\u00edas de financiamiento.",
         "text": "6422 - Actividades de las compa\u00f1\u00edas de financiamiento."
     },
     {
         "id"  : "6423 - Banca de segundo piso.",
         "text": "6423 - Banca de segundo piso."
     },
     {
         "id"  : "6424 - Actividades de las cooperativas financieras.",
         "text": "6424 - Actividades de las cooperativas financieras."
     },
     {
         "id"  : "6431 - Fideicomisos, fondos y entidades financieras similares.",
         "text": "6431 - Fideicomisos, fondos y entidades financieras similares."
     },
     {
         "id"  : "6432 - Fondos de cesant\u00edas.",
         "text": "6432 - Fondos de cesant\u00edas."
     },
     {
         "id"  : "6491 - Leasing financiero (arrendamiento financiero).",
         "text": "6491 - Leasing financiero (arrendamiento financiero)."
     },
     {
         "id"  : "6492 - Actividades financieras de fondos de empleados y otras formas asociativas del sector solidario.",
         "text": "6492 - Actividades financieras de fondos de empleados y otras formas asociativas del sector solidario."
     },
     {
         "id"  : "6493 - Actividades de compra de cartera o factoring.",
         "text": "6493 - Actividades de compra de cartera o factoring."
     },
     {
         "id"  : "6494 - Otras actividades de distribuci\u00f3n de fondos.",
         "text": "6494 - Otras actividades de distribuci\u00f3n de fondos."
     },
     {
         "id"  : "6495 - Instituciones especiales oficiales.",
         "text": "6495 - Instituciones especiales oficiales."
     },
     {
         "id"  : "6499 - Otras actividades de servicio financiero, excepto las de seguros y pensiones n.c.p.",
         "text": "6499 - Otras actividades de servicio financiero, excepto las de seguros y pensiones n.c.p."
     },
     {
         "id"  : "6511 - Seguros generales.",
         "text": "6511 - Seguros generales."
     },
     {
         "id"  : "6512 - Seguros de vida.",
         "text": "6512 - Seguros de vida."
     },
     {
         "id"  : "6513 - Reaseguros.",
         "text": "6513 - Reaseguros."
     },
     {
         "id"  : "6514 - Capitalizaci\u00f3n.",
         "text": "6514 - Capitalizaci\u00f3n."
     },
     {
         "id"  : "6521 - Servicios de seguros sociales de salud.",
         "text": "6521 - Servicios de seguros sociales de salud."
     },
     {
         "id"  : "6522 - Servicios de seguros sociales de riesgos profesionales.",
         "text": "6522 - Servicios de seguros sociales de riesgos profesionales."
     },
     {
         "id"  : "6531 - R\u00e9gimen de prima media con prestaci\u00f3n definida (RPM).",
         "text": "6531 - R\u00e9gimen de prima media con prestaci\u00f3n definida (RPM)."
     },
     {
         "id"  : "6532 - R\u00e9gimen de ahorro individual (RAI).",
         "text": "6532 - R\u00e9gimen de ahorro individual (RAI)."
     },
     {
         "id"  : "6611 - Administraci\u00f3n de mercados financieros.",
         "text": "6611 - Administraci\u00f3n de mercados financieros."
     },
     {
         "id"  : "6612 - Corretaje de valores y de contratos de productos b\u00e1sicos.",
         "text": "6612 - Corretaje de valores y de contratos de productos b\u00e1sicos."
     },
     {
         "id"  : "6613 - Otras actividades relacionadas con el mercado de valores.",
         "text": "6613 - Otras actividades relacionadas con el mercado de valores."
     },
     {
         "id"  : "6614 - Actividades de las casas de cambio.",
         "text": "6614 - Actividades de las casas de cambio."
     },
     {
         "id"  : "6615 - Actividades de los profesionales de compra y venta de divisas.",
         "text": "6615 - Actividades de los profesionales de compra y venta de divisas."
     },
     {
         "id"  : "6619 - Otras actividades auxiliares de las actividades de servicios financieros n.c.p.",
         "text": "6619 - Otras actividades auxiliares de las actividades de servicios financieros n.c.p."
     },
     {
         "id"  : "6621 - Actividades de agentes y corredores de seguros",
         "text": "6621 - Actividades de agentes y corredores de seguros"
     },
     {
         "id"  : "6629 - Evaluaci\u00f3n de riesgos y da\u00f1os, y otras actividades de servicios auxiliares",
         "text": "6629 - Evaluaci\u00f3n de riesgos y da\u00f1os, y otras actividades de servicios auxiliares"
     },
     {
         "id"  : "6630 - Actividades de administraci\u00f3n de fondos.",
         "text": "6630 - Actividades de administraci\u00f3n de fondos."
     },
     {
         "id"  : "6810 - Actividades inmobiliarias realizadas con bienes propios o arrendados.",
         "text": "6810 - Actividades inmobiliarias realizadas con bienes propios o arrendados."
     },
     {
         "id"  : "6820 - Actividades inmobiliarias realizadas a cambio de una retribuci\u00f3n o por contrata.",
         "text": "6820 - Actividades inmobiliarias realizadas a cambio de una retribuci\u00f3n o por contrata."
     },
     {
         "id"  : "6910 - Actividades jur\u00eddicas.",
         "text": "6910 - Actividades jur\u00eddicas."
     },
     {
         "id"  : "6920 - Actividades de contabilidad, tenedur\u00eda de libros, auditor\u00eda financiera y asesor\u00eda tributaria.",
         "text": "6920 - Actividades de contabilidad, tenedur\u00eda de libros, auditor\u00eda financiera y asesor\u00eda tributaria."
     },
     {
         "id"  : "7010 - Actividades de administraci\u00f3n empresarial.",
         "text": "7010 - Actividades de administraci\u00f3n empresarial."
     },
     {
         "id"  : "7020 - Actividades de consultar\u00eda de gesti\u00f3n.",
         "text": "7020 - Actividades de consultar\u00eda de gesti\u00f3n."
     },
     {
         "id"  : "7110 - Actividades de arquitectura e ingenier\u00eda y otras actividades conexas de consultor\u00eda t\u00e9cnica.",
         "text": "7110 - Actividades de arquitectura e ingenier\u00eda y otras actividades conexas de consultor\u00eda t\u00e9cnica."
     },
     {
         "id"  : "7120 - Ensayos y an\u00e1lisis t\u00e9cnicos.",
         "text": "7120 - Ensayos y an\u00e1lisis t\u00e9cnicos."
     },
     {
         "id"  : "7210 - Investigaciones y desarrollo experimental en el campo de las ciencias naturales y la ingenier\u00eda.",
         "text": "7210 - Investigaciones y desarrollo experimental en el campo de las ciencias naturales y la ingenier\u00eda."
     },
     {
         "id"  : "7220 - Investigaciones y desarrollo experimental en el campo de las ciencias sociales y las humanidades.",
         "text": "7220 - Investigaciones y desarrollo experimental en el campo de las ciencias sociales y las humanidades."
     },
     {
         "id"  : "7310 - Publicidad.",
         "text": "7310 - Publicidad."
     },
     {
         "id"  : "7320 - Estudios de mercado y realizaci\u00f3n de encuestas de opini\u00f3n p\u00fablica.",
         "text": "7320 - Estudios de mercado y realizaci\u00f3n de encuestas de opini\u00f3n p\u00fablica."
     },
     {
         "id"  : "7410 - Actividades especializadas de dise\u00f1o.",
         "text": "7410 - Actividades especializadas de dise\u00f1o."
     },
     {
         "id"  : "7420 - Actividades de fotograf\u00eda.",
         "text": "7420 - Actividades de fotograf\u00eda."
     },
     {
         "id"  : "7490 - Otras actividades profesionales, cient\u00edficas y t\u00e9cnicas n.c.p.",
         "text": "7490 - Otras actividades profesionales, cient\u00edficas y t\u00e9cnicas n.c.p."
     },
     {
         "id"  : "7500 - Actividades veterinarias.",
         "text": "7500 - Actividades veterinarias."
     },
     {
         "id"  : "7710 - Alquiler y arrendamiento de veh\u00edculos automotores.",
         "text": "7710 - Alquiler y arrendamiento de veh\u00edculos automotores."
     },
     {
         "id"  : "7721 - Alquiler y arrendamiento de equipo recreativo y deportivo.",
         "text": "7721 - Alquiler y arrendamiento de equipo recreativo y deportivo."
     },
     {
         "id"  : "7722 - Alquiler de videos y discos.",
         "text": "7722 - Alquiler de videos y discos."
     },
     {
         "id"  : "7729 - Alquiler y arrendamiento de otros efectos personales y enseres dom\u00e9sticos n.c.p.",
         "text": "7729 - Alquiler y arrendamiento de otros efectos personales y enseres dom\u00e9sticos n.c.p."
     },
     {
         "id"  : "7730 - Alquiler y arrendamiento de otros tipos de maquinaria, equipo y bienes tangibles n.c.p.",
         "text": "7730 - Alquiler y arrendamiento de otros tipos de maquinaria, equipo y bienes tangibles n.c.p."
     },
     {
         "id"  : "7740 - Arrendamiento de propiedad intelectual y productos similares, excepto obras protegidas por derechos de autor.",
         "text": "7740 - Arrendamiento de propiedad intelectual y productos similares, excepto obras protegidas por derechos de autor."
     },
     {
         "id"  : "7810 - Actividades de agencias de empleo.",
         "text": "7810 - Actividades de agencias de empleo."
     },
     {
         "id"  : "7820 - Actividades de agencias de empleo temporal.",
         "text": "7820 - Actividades de agencias de empleo temporal."
     },
     {
         "id"  : "7830 - Otras actividades de suministro de recurso humano.",
         "text": "7830 - Otras actividades de suministro de recurso humano."
     },
     {
         "id"  : "7911 - Actividades de las agencias de viaje.",
         "text": "7911 - Actividades de las agencias de viaje."
     },
     {
         "id"  : "7912 - Actividades de operadores tur\u00edsticos.",
         "text": "7912 - Actividades de operadores tur\u00edsticos."
     },
     {
         "id"  : "7990 - Otros servicios de reserva y actividades relacionadas.",
         "text": "7990 - Otros servicios de reserva y actividades relacionadas."
     },
     {
         "id"  : "8010 - Actividades de seguridad privada.",
         "text": "8010 - Actividades de seguridad privada."
     },
     {
         "id"  : "8020 - Actividades de servicios de sistemas de seguridad.",
         "text": "8020 - Actividades de servicios de sistemas de seguridad."
     },
     {
         "id"  : "8030 - Actividades de detectives e investigadores privados.",
         "text": "8030 - Actividades de detectives e investigadores privados."
     },
     {
         "id"  : "8110 - Actividades combinadas de apoyo a instalaciones.",
         "text": "8110 - Actividades combinadas de apoyo a instalaciones."
     },
     {
         "id"  : "8121 - Limpieza general interior de edificios.",
         "text": "8121 - Limpieza general interior de edificios."
     },
     {
         "id"  : "8129 - Otras actividades de limpieza de edificios e instalaciones industriales.",
         "text": "8129 - Otras actividades de limpieza de edificios e instalaciones industriales."
     },
     {
         "id"  : "8130 - Actividades de paisajismo y servicios de mantenimiento conexos.",
         "text": "8130 - Actividades de paisajismo y servicios de mantenimiento conexos."
     },
     {
         "id"  : "8211 - Actividades combinadas de servicios administrativos de oficina.",
         "text": "8211 - Actividades combinadas de servicios administrativos de oficina."
     },
     {
         "id"  : "8219 - Fotocopiado, preparaci\u00f3n de documentos y otras actividades especializadas de apoyo a oficina.",
         "text": "8219 - Fotocopiado, preparaci\u00f3n de documentos y otras actividades especializadas de apoyo a oficina."
     },
     {
         "id"  : "8220 - Actividades de centros de llamadas (Call center).",
         "text": "8220 - Actividades de centros de llamadas (Call center)."
     },
     {
         "id"  : "8230 - Organizaci\u00f3n de convenciones y eventos comerciales.",
         "text": "8230 - Organizaci\u00f3n de convenciones y eventos comerciales."
     },
     {
         "id"  : "8291 - Actividades de agencias de cobranza y oficinas de calificaci\u00f3n crediticia.",
         "text": "8291 - Actividades de agencias de cobranza y oficinas de calificaci\u00f3n crediticia."
     },
     {
         "id"  : "8292 - Actividades de envase y empaque.",
         "text": "8292 - Actividades de envase y empaque."
     },
     {
         "id"  : "8299 - Otras actividades de servicio de apoyo a las empresas n.c.p.",
         "text": "8299 - Otras actividades de servicio de apoyo a las empresas n.c.p."
     },
     {
         "id"  : "8411 - Actividades legislativas de la administraci\u00f3n p\u00fablica.",
         "text": "8411 - Actividades legislativas de la administraci\u00f3n p\u00fablica."
     },
     {
         "id"  : "8412 - Actividades ejecutivas de la administraci\u00f3n p\u00fablica.",
         "text": "8412 - Actividades ejecutivas de la administraci\u00f3n p\u00fablica."
     },
     {
         "id"  : "8413 - Regulaci\u00f3n de las actividades de organismos que prestan servicios de salud, educativos, culturales y otros servicios sociales, excepto servicios de seguridad social.",
         "text": "8413 - Regulaci\u00f3n de las actividades de organismos que prestan servicios de salud, educativos, culturales y otros servicios sociales, excepto servicios de seguridad social."
     },
     {
         "id"  : "8414 - Actividades reguladoras y facilitadoras de la actividad econ\u00f3mica.",
         "text": "8414 - Actividades reguladoras y facilitadoras de la actividad econ\u00f3mica."
     },
     {
         "id"  : "8415 - Actividades de los otros \u00f3rganos de control.",
         "text": "8415 - Actividades de los otros \u00f3rganos de control."
     },
     {
         "id"  : "8421 - Relaciones exteriores.",
         "text": "8421 - Relaciones exteriores."
     },
     {
         "id"  : "8422 - Actividades de defensa.",
         "text": "8422 - Actividades de defensa."
     },
     {
         "id"  : "8423 - Orden p\u00fablico y actividades de seguridad.",
         "text": "8423 - Orden p\u00fablico y actividades de seguridad."
     },
     {
         "id"  : "8424 - Administraci\u00f3n de justicia.",
         "text": "8424 - Administraci\u00f3n de justicia."
     },
     {
         "id"  : "8430 - Actividades de planes de seguridad social de afiliaci\u00f3n obligatoria.",
         "text": "8430 - Actividades de planes de seguridad social de afiliaci\u00f3n obligatoria."
     },
     {
         "id"  : "8511 - Educaci\u00f3n de la primera infancia.",
         "text": "8511 - Educaci\u00f3n de la primera infancia."
     },
     {
         "id"  : "8512 - Educaci\u00f3n preescolar.",
         "text": "8512 - Educaci\u00f3n preescolar."
     },
     {
         "id"  : "8513 - Educaci\u00f3n b\u00e1sica primaria.",
         "text": "8513 - Educaci\u00f3n b\u00e1sica primaria."
     },
     {
         "id"  : "8521 - Educaci\u00f3n b\u00e1sica secundaria.",
         "text": "8521 - Educaci\u00f3n b\u00e1sica secundaria."
     },
     {
         "id"  : "8522 - Educaci\u00f3n media acad\u00e9mica.",
         "text": "8522 - Educaci\u00f3n media acad\u00e9mica."
     },
     {
         "id"  : "8523 - Educaci\u00f3n media t\u00e9cnica y de formaci\u00f3n laboral.",
         "text": "8523 - Educaci\u00f3n media t\u00e9cnica y de formaci\u00f3n laboral."
     },
     {
         "id"  : "8530 - Establecimientos que combinan diferentes niveles de educaci\u00f3n.",
         "text": "8530 - Establecimientos que combinan diferentes niveles de educaci\u00f3n."
     },
     {
         "id"  : "8541 - Educaci\u00f3n t\u00e9cnica profesional.",
         "text": "8541 - Educaci\u00f3n t\u00e9cnica profesional."
     },
     {
         "id"  : "8542 - Educaci\u00f3n tecnol\u00f3gica.",
         "text": "8542 - Educaci\u00f3n tecnol\u00f3gica."
     },
     {
         "id"  : "8543 - Educaci\u00f3n de instituciones universitarias o de escuelas tecnol\u00f3gicas.",
         "text": "8543 - Educaci\u00f3n de instituciones universitarias o de escuelas tecnol\u00f3gicas."
     },
     {
         "id"  : "8544 - Educaci\u00f3n de universidades.",
         "text": "8544 - Educaci\u00f3n de universidades."
     },
     {
         "id"  : "8551 - Formaci\u00f3n acad\u00e9mica no formal.",
         "text": "8551 - Formaci\u00f3n acad\u00e9mica no formal."
     },
     {
         "id"  : "8552 - Ense\u00f1anza deportiva y recreativa.",
         "text": "8552 - Ense\u00f1anza deportiva y recreativa."
     },
     {
         "id"  : "8553 - Ense\u00f1anza cultural.",
         "text": "8553 - Ense\u00f1anza cultural."
     },
     {
         "id"  : "8559 - Otros tipos de educaci\u00f3n n.c.p.",
         "text": "8559 - Otros tipos de educaci\u00f3n n.c.p."
     },
     {
         "id"  : "8560 - Actividades de apoyo a la educaci\u00f3n.",
         "text": "8560 - Actividades de apoyo a la educaci\u00f3n."
     },
     {
         "id"  : "8610 - Actividades de hospitales y cl\u00ednicas, con internaci\u00f3n.",
         "text": "8610 - Actividades de hospitales y cl\u00ednicas, con internaci\u00f3n."
     },
     {
         "id"  : "8621 - Actividades de la pr\u00e1ctica m\u00e9dica, sin internaci\u00f3n.",
         "text": "8621 - Actividades de la pr\u00e1ctica m\u00e9dica, sin internaci\u00f3n."
     },
     {
         "id"  : "8622 - Actividades de la pr\u00e1ctica odontol\u00f3gica.",
         "text": "8622 - Actividades de la pr\u00e1ctica odontol\u00f3gica."
     },
     {
         "id"  : "8691 - Actividades de apoyo diagn\u00f3stico.",
         "text": "8691 - Actividades de apoyo diagn\u00f3stico."
     },
     {
         "id"  : "8692 - Actividades de apoyo terap\u00e9utico.",
         "text": "8692 - Actividades de apoyo terap\u00e9utico."
     },
     {
         "id"  : "8699 - Otras actividades de atenci\u00f3n de la salud humana.",
         "text": "8699 - Otras actividades de atenci\u00f3n de la salud humana."
     },
     {
         "id"  : "8710 - Actividades de atenci\u00f3n residencial medicalizada de tipo general.",
         "text": "8710 - Actividades de atenci\u00f3n residencial medicalizada de tipo general."
     },
     {
         "id"  : "8720 - Actividades de atenci\u00f3n residencial, para el cuidado de pacientes con retardo mental, enfermedad mental y consumo de sustancias psicoactivas.",
         "text": "8720 - Actividades de atenci\u00f3n residencial, para el cuidado de pacientes con retardo mental, enfermedad mental y consumo de sustancias psicoactivas."
     },
     {
         "id"  : "8730 - Actividades de atenci\u00f3n en instituciones para el cuidado de personas mayores y\/o discapacitadas.",
         "text": "8730 - Actividades de atenci\u00f3n en instituciones para el cuidado de personas mayores y\/o discapacitadas."
     },
     {
         "id"  : "8790 - Otras actividades de atenci\u00f3n en instituciones con alojamiento",
         "text": "8790 - Otras actividades de atenci\u00f3n en instituciones con alojamiento"
     },
     {
         "id"  : "8810 - Actividades de asistencia social sin alojamiento para personas mayores y discapacitadas.",
         "text": "8810 - Actividades de asistencia social sin alojamiento para personas mayores y discapacitadas."
     },
     {
         "id"  : "8890 - Otras actividades de asistencia social sin alojamiento.",
         "text": "8890 - Otras actividades de asistencia social sin alojamiento."
     },
     {
         "id"  : "9001 - Creaci\u00f3n literaria.",
         "text": "9001 - Creaci\u00f3n literaria."
     },
     {
         "id"  : "9002 - Creaci\u00f3n musical.",
         "text": "9002 - Creaci\u00f3n musical."
     },
     {
         "id"  : "9003 - Creaci\u00f3n teatral.",
         "text": "9003 - Creaci\u00f3n teatral."
     },
     {
         "id"  : "9004 - Creaci\u00f3n audiovisual.",
         "text": "9004 - Creaci\u00f3n audiovisual."
     },
     {
         "id"  : "9005 - Artes pl\u00e1sticas y visuales.",
         "text": "9005 - Artes pl\u00e1sticas y visuales."
     },
     {
         "id"  : "9006 - Actividades teatrales.",
         "text": "9006 - Actividades teatrales."
     },
     {
         "id"  : "9007 - Actividades de espect\u00e1culos musicales en vivo.",
         "text": "9007 - Actividades de espect\u00e1culos musicales en vivo."
     },
     {
         "id"  : "9008 - Otras actividades de espect\u00e1culos en vivo.",
         "text": "9008 - Otras actividades de espect\u00e1culos en vivo."
     },
     {
         "id"  : "9101 - Actividades de bibliotecas y archivos.",
         "text": "9101 - Actividades de bibliotecas y archivos."
     },
     {
         "id"  : "9102 - Actividades y funcionamiento de museos, conservaci\u00f3n de edificios y sitios hist\u00f3ricos.",
         "text": "9102 - Actividades y funcionamiento de museos, conservaci\u00f3n de edificios y sitios hist\u00f3ricos."
     },
     {
         "id"  : "9103 - Actividades de jardines bot\u00e1nicos, zool\u00f3gicos y reservas naturales.",
         "text": "9103 - Actividades de jardines bot\u00e1nicos, zool\u00f3gicos y reservas naturales."
     },
     {
         "id"  : "9200 - Actividades de juegos de azar y apuestas.",
         "text": "9200 - Actividades de juegos de azar y apuestas."
     },
     {
         "id"  : "9311 - Gesti\u00f3n de instalaciones deportivas.",
         "text": "9311 - Gesti\u00f3n de instalaciones deportivas."
     },
     {
         "id"  : "9312 - Actividades de clubes deportivos.",
         "text": "9312 - Actividades de clubes deportivos."
     },
     {
         "id"  : "9319 - Otras actividades deportivas.",
         "text": "9319 - Otras actividades deportivas."
     },
     {
         "id"  : "9321 - Actividades de parques de atracciones y parques tem\u00e1ticos.",
         "text": "9321 - Actividades de parques de atracciones y parques tem\u00e1ticos."
     },
     {
         "id"  : "9329 - Otras actividades recreativas y de esparcimiento n.c.p.",
         "text": "9329 - Otras actividades recreativas y de esparcimiento n.c.p."
     },
     {
         "id"  : "9411 - Actividades de asociaciones empresariales y de empleadores",
         "text": "9411 - Actividades de asociaciones empresariales y de empleadores"
     },
     {
         "id"  : "9412 - Actividades de asociaciones profesionales",
         "text": "9412 - Actividades de asociaciones profesionales"
     },
     {
         "id"  : "9420 - Actividades de sindicatos de empleados.",
         "text": "9420 - Actividades de sindicatos de empleados."
     },
     {
         "id"  : "9491 - Actividades de asociaciones religiosas.",
         "text": "9491 - Actividades de asociaciones religiosas."
     },
     {
         "id"  : "9492 - Actividades de asociaciones pol\u00edticas.",
         "text": "9492 - Actividades de asociaciones pol\u00edticas."
     },
     {
         "id"  : "9499 - Actividades de otras asociaciones n.c.p.",
         "text": "9499 - Actividades de otras asociaciones n.c.p."
     },
     {
         "id"  : "9511 - Mantenimiento y reparaci\u00f3n de computadores y de equipo perif\u00e9rico.",
         "text": "9511 - Mantenimiento y reparaci\u00f3n de computadores y de equipo perif\u00e9rico."
     },
     {
         "id"  : "9512 - Mantenimiento y reparaci\u00f3n de equipos de comunicaci\u00f3n.",
         "text": "9512 - Mantenimiento y reparaci\u00f3n de equipos de comunicaci\u00f3n."
     },
     {
         "id"  : "9521 - Mantenimiento y reparaci\u00f3n de aparatos electr\u00f3nicos de consumo.",
         "text": "9521 - Mantenimiento y reparaci\u00f3n de aparatos electr\u00f3nicos de consumo."
     },
     {
         "id"  : "9522 - Mantenimiento y reparaci\u00f3n de aparatos y equipos dom\u00e9sticos y de jardiner\u00eda.",
         "text": "9522 - Mantenimiento y reparaci\u00f3n de aparatos y equipos dom\u00e9sticos y de jardiner\u00eda."
     },
     {
         "id"  : "9523 - Reparaci\u00f3n de calzado y art\u00edculos de cuero.",
         "text": "9523 - Reparaci\u00f3n de calzado y art\u00edculos de cuero."
     },
     {
         "id"  : "9524 - Reparaci\u00f3n de muebles y accesorios para el hogar.",
         "text": "9524 - Reparaci\u00f3n de muebles y accesorios para el hogar."
     },
     {
         "id"  : "9529 - Mantenimiento y reparaci\u00f3n de otros efectos personales y enseres dom\u00e9sticos.",
         "text": "9529 - Mantenimiento y reparaci\u00f3n de otros efectos personales y enseres dom\u00e9sticos."
     },
     {
         "id"  : "9601 - Lavado y limpieza, incluso la limpieza en seco, de productos textiles y de piel.",
         "text": "9601 - Lavado y limpieza, incluso la limpieza en seco, de productos textiles y de piel."
     },
     {
         "id"  : "9602 - Peluquer\u00eda y otros tratamientos de belleza.",
         "text": "9602 - Peluquer\u00eda y otros tratamientos de belleza."
     },
     {
         "id"  : "9603 - Pompas f\u00fanebres y actividades relacionadas.",
         "text": "9603 - Pompas f\u00fanebres y actividades relacionadas."
     },
     {
         "id"  : "9609 - Otras actividades de servicios personales n.c.p.",
         "text": "9609 - Otras actividades de servicios personales n.c.p."
     },
     {
         "id"  : "9700 - Actividades de los hogares individuales como empleadores de personal dom\u00e9stico.",
         "text": "9700 - Actividades de los hogares individuales como empleadores de personal dom\u00e9stico."
     },
     {
         "id"  : "9810 - Actividades no diferenciadas de los hogares individuales como productores de bienes para uso propio.",
         "text": "9810 - Actividades no diferenciadas de los hogares individuales como productores de bienes para uso propio."
     },
     {
         "id"  : "9820 - Actividades no diferenciadas de los hogares individuales como productores de servicios para uso propio.",
         "text": "9820 - Actividades no diferenciadas de los hogares individuales como productores de servicios para uso propio."
     },
     {
         "id"  : "9900 - Actividades de organizaciones y entidades extraterritoriales.",
         "text": "9900 - Actividades de organizaciones y entidades extraterritoriales."
     }], "more"
:
    false, "limit"
:
    0
}
